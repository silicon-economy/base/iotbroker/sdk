/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.distribution;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author smengers
 */
public class DistributionHelperTest {

    public DistributionHelperTest() {
    }

    private static <T> Set<T> subSet(Set<T> set, int begin, int end) {
        List<T> list = Arrays.asList((T[]) set.toArray());
        return new HashSet(list.subList(begin, end));
    }

    @Test
    public void testCalculateDistributionOneKey() {

        Set<String> objects = new HashSet<>();
        for (int i = 0; i < 20; i++) {
            objects.add(i + "");
        }
        List<String> keys = new ArrayList<>();
        keys.add("1");

        Map result = DistributionHelper.calculateDistribution(keys, objects);

        Map<String, Set<String>> expectedResult = new HashMap<>();
        expectedResult.put("1", objects);

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void testCalculateDistributionTwoKeysEvenObjectsCount() {
        Set<String> objects = new HashSet<>();
        int objectsSize = 20;

        for (int i = 0; i < objectsSize; i++) {
            objects.add(i + "");
        }
        List<String> keys = new ArrayList<>();
        keys.add("1");
        keys.add("2");

        Map result = DistributionHelper.calculateDistribution(keys, objects);

        Map<String, Set<String>> expectedResult = new HashMap<>();
        expectedResult.put("1", subSet(objects, 0, objectsSize / 2));
        expectedResult.put("2", subSet(objects, objectsSize / 2, objects.size()));

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void testCalculateDistributionFiveKeysPrimObjectCount() {
        Set<String> objects = new HashSet<>();
        int objectsSize = 39;

        for (int i = 0; i < objectsSize; i++) {
            objects.add(i + "");
        }
        List<String> keys = new ArrayList<>();
        keys.add("1");
        keys.add("2");
        keys.add("3");
        keys.add("4");
        keys.add("5");

        Map result = DistributionHelper.calculateDistribution(keys, objects);

        Map<String, Set<String>> expectedResult = new HashMap<>();
        expectedResult.put("1", subSet(objects, 0, objectsSize / 5 + 1));
        expectedResult.put("2", subSet(objects, objectsSize / 5 + 1, objectsSize / 5 * 2 + 2));
        expectedResult.put("3", subSet(objects, objectsSize / 5 * 2 + 2, objectsSize / 5 * 3 + 3));
        expectedResult.put("4", subSet(objects, objectsSize / 5 * 3 + 3, objectsSize / 5 * 4 + 4));
        expectedResult.put("5", subSet(objects, objectsSize / 5 * 4 + 4, objectsSize));

        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void testCalculateDistributionLessObjectsThanKeys() {
        Set<String> objects = new HashSet<>();
        int objectsSize = 2;

        for (int i = 0; i < objectsSize; i++) {
            objects.add(i + "");
        }
        List<String> keys = new ArrayList<>();
        keys.add("1");
        keys.add("2");
        keys.add("3");
        keys.add("4");

        Map result = DistributionHelper.calculateDistribution(keys, objects);

        Map<String, Set<String>> expectedResult = new HashMap<>();
        expectedResult.put("1", subSet(objects, 0, 1));
        expectedResult.put("2", subSet(objects, 1, 2));
        expectedResult.put("3", new HashSet<>());
        expectedResult.put("4", new HashSet<>());

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void testCalculateDistributionNoKeys(){
        Set<String> objects = new HashSet<>();
        objects.add("1");

        List<String> keys = new ArrayList<>();

        Map result = DistributionHelper.calculateDistribution(keys, objects);

        Map<String, Set<String>> expectedResult = new HashMap<>();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void testCalculateDistributionOneKeyNoObjects(){
        Set<String> objects = new HashSet<>();

        List<String> keys = new ArrayList<>();
        keys.add("1");

        Map result = DistributionHelper.calculateDistribution(keys, objects);

        Map<String, Set<String>> expectedResult = new HashMap<>();
        expectedResult.put("1", new HashSet<>() );

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void testCalculateDistributionEqualKeysAndObjects(){
        Set<String> objects = new HashSet<>();
        objects.add("1");
        objects.add("2");

        List<String> keys = new ArrayList<>();
        keys.add("1");
        keys.add("2");

        Map result = DistributionHelper.calculateDistribution(keys, objects);

        Map<String, Set<String>> expectedResult = new HashMap<>();
        expectedResult.put("1", subSet(objects, 0,1));
        expectedResult.put("2", subSet(objects,1, objects.size()));

        Assert.assertEquals(expectedResult, result);
    }

    private static void printArray( Object[] arr){
        System.out.print("[");
        for(Object o : arr){
            System.out.print(o + ",");
        }
        System.out.print("]");
        System.out.println();
    }

}
