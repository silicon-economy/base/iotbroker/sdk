/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.device;

import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate.Type.*;

/**
 * Test cases for {@link DeviceInstanceUpdate}.
 *
 * @author M. Grzenia
 */
class DeviceInstanceUpdateTest {

    @Test
    void shouldSucceedObjectInstantiation() {
        assertDoesNotThrow(() -> {
            new DeviceInstanceUpdate(createDummyInstance(), null, CREATED);
            new DeviceInstanceUpdate(createDummyInstance(), createDummyInstance(), MODIFIED);
            new DeviceInstanceUpdate(null, createDummyInstance(), DELETED);
        });
    }

    @Test
    void shouldFailObjectInstantiation() {
        assertThrows(
            IllegalArgumentException.class,
            () -> new DeviceInstanceUpdate(null, null, CREATED)
        );

        assertThrows(
            IllegalArgumentException.class,
            () -> new DeviceInstanceUpdate(null, null, MODIFIED)
        );

        assertThrows(
            IllegalArgumentException.class,
            () -> new DeviceInstanceUpdate(null, null, DELETED)
        );
    }

    private DeviceInstance createDummyInstance() {
        return new DeviceInstance("", "", "", "", Instant.now(),
            Instant.EPOCH, false, "", "", "");
    }
}
