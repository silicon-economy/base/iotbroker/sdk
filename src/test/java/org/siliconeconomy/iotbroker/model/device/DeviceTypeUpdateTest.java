/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.device;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate.Type.*;

/**
 * Test cases for {@link DeviceTypeUpdate}.
 *
 * @author M. Grzenia
 */
class DeviceTypeUpdateTest {

    @Test
    void shouldSucceedObjectInstantiation() {
        assertDoesNotThrow(() -> {
            new DeviceTypeUpdate(createDummyInstance(), null, CREATED);
            new DeviceTypeUpdate(createDummyInstance(), createDummyInstance(), MODIFIED);
            new DeviceTypeUpdate(null, createDummyInstance(), DELETED);
        });
    }

    @Test
    void shouldFailObjectInstantiation() {
        assertThrows(
            IllegalArgumentException.class,
            () -> new DeviceTypeUpdate(null, null, CREATED)
        );

        assertThrows(
            IllegalArgumentException.class,
            () -> new DeviceTypeUpdate(null, null, MODIFIED)
        );

        assertThrows(
            IllegalArgumentException.class,
            () -> new DeviceTypeUpdate(null, null, DELETED)
        );
    }

    private DeviceType createDummyInstance() {
        return new DeviceType("id", "", "", Set.of(), "", false, false, false);
    }
}
