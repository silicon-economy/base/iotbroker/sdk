/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.sensordata.serializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.siliconeconomy.iotbroker.model.sensordata.Datastream;
import org.siliconeconomy.iotbroker.model.sensordata.LocationDetails;
import org.siliconeconomy.iotbroker.model.sensordata.ObservationResult;
import org.siliconeconomy.iotbroker.model.sensordata.location.LocationEncodingType;
import org.siliconeconomy.iotbroker.model.sensordata.observation.ObservationType;
import org.siliconeconomy.iotbroker.utils.sensordata.SensorDataModule;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * Test cases for {@link ClassSerializer}.
 *
 * @author M. Grzenia
 */
class ClassSerializerTest {

    private ObjectMapper mapper;

    @BeforeEach
    void setUp() {
        mapper = new ObjectMapper();
        mapper.registerModule(new SensorDataModule());
    }

    @ParameterizedTest
    @EnumSource(LocationEncodingType.class)
    void shouldSerializeLocationDetailsSubTypesToEnumNames(LocationEncodingType locationEncodingType) {
        assertThat(mapper.convertValue(locationEncodingType, String.class)).isEqualTo(locationEncodingType.name());
    }

    @ParameterizedTest
    @EnumSource(ObservationType.class)
    void shouldSerializeObservationResultSubTypesToEnumNames(ObservationType observationType) {
        assertThat(mapper.convertValue(observationType, String.class)).isEqualTo(observationType.name());
    }

    @Test
    void shoudlFailForLocationDetailsNotCoveredByEnum() {
        assertThatThrownBy(() -> mapper.convertValue(SomeNewLocationDetails.class, String.class))
            .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void shouldFailForObservationResultNotCoveredByEnum() {
        assertThatThrownBy(() -> mapper.convertValue(SomeNewObservationResult.class, String.class))
            .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void shouldSerializeOtherClassesByCanonicalName() {
        assertThat(mapper.convertValue(String.class, String.class)).isEqualTo(String.class.getCanonicalName());
        assertThat(mapper.convertValue(Integer.class, String.class)).isEqualTo(Integer.class.getCanonicalName());
        assertThat(mapper.convertValue(Datastream.class, String.class)).isEqualTo(Datastream.class.getCanonicalName());
    }

    private class SomeNewLocationDetails implements LocationDetails<String> {
        @Override
        public @NonNull String getDetails() {
            return "The details.";
        }
    }

    private class SomeNewObservationResult implements ObservationResult<String> {
        @Override
        public @Nullable String getResult() {
            return "The result.";
        }
    }
}
