/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.sensordata;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * A test for deserialization of {@link SensorDataMessage} instances.
 *
 * @author M. Grzenia
 */
class DeserializationTest {

    private ObjectMapper mapper;
    /**
     * A separate mapper for JSON parsing (to rule out any potential side effects regarding the main mapper's
     * configuration).
     */
    private ObjectMapper unconfiguredMapper;

    @BeforeEach
    void setUp() {
        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.registerModule(new SensorDataModule());
        unconfiguredMapper = new ObjectMapper();
    }

    @Test
    void deserialize() throws IOException {
        // Arrange
        String path = "src/test/resources/org/siliconeconomy/iotbroker/utils/sensordata/sensor-data-model.json";
        String jsonString = unconfiguredMapper.readTree(new File(path)).toString();

        // Act
        SensorDataMessage deserializationResult = mapper.readValue(jsonString, SensorDataMessage.class);

        // Assert
        assertEquals(new ExampleSensorDataMessage(), deserializationResult);
    }
}
