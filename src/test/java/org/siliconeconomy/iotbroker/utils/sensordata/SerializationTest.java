/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.sensordata;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * A test for serialization of {@link SensorDataMessage} instances.
 *
 * @author M. Grzenia
 */
class SerializationTest {

    private ObjectMapper mapper;
    /**
     * A separate mapper for JSON parsing (to rule out any potential side effects regarding the main mapper's
     * configuration).
     */
    private ObjectMapper unconfiguredMapper;

    @BeforeEach
    void setUp() {
        mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.registerModule(new JavaTimeModule());
        mapper.registerModule(new SensorDataModule());
        unconfiguredMapper = new ObjectMapper();
    }

    @Test
    void serialize() throws IOException {
        // Arrange
        SensorDataMessage model = new ExampleSensorDataMessage();

        // Act
        String serializationResult = mapper.writeValueAsString(model);

        // Assert
        JsonNode resultNode = unconfiguredMapper.readTree(serializationResult);

        String path = "src/test/resources/org/siliconeconomy/iotbroker/utils/sensordata/sensor-data-model.json";
        JsonNode expectedNode = unconfiguredMapper.readTree(new File(path));

        assertEquals(expectedNode, resultNode);
    }
}
