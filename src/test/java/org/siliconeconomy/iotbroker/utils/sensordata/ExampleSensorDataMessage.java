/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.sensordata;

import org.assertj.core.util.Maps;
import org.siliconeconomy.iotbroker.model.sensordata.*;
import org.siliconeconomy.iotbroker.model.sensordata.location.LatitudeLongitudeDetails;
import org.siliconeconomy.iotbroker.model.sensordata.location.MapcodeDetails;
import org.siliconeconomy.iotbroker.model.sensordata.location.SimpleDetails;
import org.siliconeconomy.iotbroker.model.sensordata.location.datatype.LatLong;
import org.siliconeconomy.iotbroker.model.sensordata.location.datatype.Mapcode;
import org.siliconeconomy.iotbroker.model.sensordata.observation.CountResult;
import org.siliconeconomy.iotbroker.model.sensordata.observation.MeasurementResult;
import org.siliconeconomy.iotbroker.model.sensordata.observation.SimpleResult;
import org.siliconeconomy.iotbroker.model.sensordata.observation.TruthResult;

import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Sample data for testing purposes.
 *
 * @author M. Grzenia
 */
public class ExampleSensorDataMessage extends SensorDataMessage {

    private static final Instant TIME = Instant.ofEpochMilli(1625569613015L);

    public ExampleSensorDataMessage() {
        super(
            "message1",
            "deviceMessage1",
            "source1",
            "tenant1",
            TIME,
            new Location<>(
                "Tenant 1 location name",
                "Tenant 1 location description",
                SimpleDetails.class,
                new SimpleDetails("A comfy place.")
            ),
            createDatastreams());
    }

    private static List<Datastream<? extends ObservationResult<?>>> createDatastreams() {
        return Arrays.asList(
            createCountDatastream(),
            createMeasurementDatastream(),
            createTruthDatastream(),
            createSimpleDatastream()
        );
    }

    private static Datastream<CountResult> createCountDatastream() {
        Location<SimpleDetails> location = new Location<>(
            "Observation location name (simple)",
            "Observation location description (simple)",
            SimpleDetails.class,
            new SimpleDetails("A simple location.")
        );

        Observation<CountResult> observation = new Observation<>(
            TIME,
            new CountResult(1L),
            location,
            Maps.newHashMap("paramKey", "paramValue")
        );

        return new Datastream<>(
            new ObservedProperty("Property 1", "Property 1 description"),
            CountResult.class,
            new UnitOfMeasurement("Count of property 1", ""),
            Arrays.asList(observation)
        );
    }

    private static Datastream<MeasurementResult> createMeasurementDatastream() {
        Location<LatitudeLongitudeDetails> location = new Location<>(
            "Observation location name (latlong)",
            "Observation location description (latlong)",
            LatitudeLongitudeDetails.class,
            new LatitudeLongitudeDetails(new LatLong(10.0, 20.0))
        );

        Observation<MeasurementResult> observation = new Observation<>(
            TIME,
            new MeasurementResult(2.0),
            location,
            new HashMap<>()
        );

        return new Datastream<>(
            new ObservedProperty("Property 2", "Property 2 description"),
            MeasurementResult.class,
            new UnitOfMeasurement("Measurement of property 2", "Measurement symbol"),
            Arrays.asList(observation)
        );
    }

    private static Datastream<TruthResult> createTruthDatastream() {
        Location<MapcodeDetails> location = new Location<>(
            "Observation location name (mapcode)",
            "Observation location description (mapcode)",
            MapcodeDetails.class,
            new MapcodeDetails(new Mapcode("Observation territory", "Observation code"))
        );

        Observation<TruthResult> observation = new Observation<>(
            TIME,
            new TruthResult(true),
            location,
            Maps.newHashMap("paramKey", "paramValue")
        );

        return new Datastream<>(
            new ObservedProperty("Property 3", "Property 3 description"),
            TruthResult.class,
            new UnitOfMeasurement("Truth of property 3", ""),
            Arrays.asList(observation)
        );
    }

    private static Datastream<SimpleResult> createSimpleDatastream() {
        Location<LatitudeLongitudeDetails> location = new Location<>(
            "Observation location name (latlong)",
            "Observation location description (latlong)",
            LatitudeLongitudeDetails.class,
            new LatitudeLongitudeDetails(new LatLong(10.0, 20.0))
        );

        Observation<SimpleResult> observation = new Observation<>(
            TIME,
            new SimpleResult("The result is this observation's location."),
            location,
            Maps.newHashMap("paramKey", "paramValue")
        );

        return new Datastream<>(
            new ObservedProperty("Property 4", "Property 4 description"),
            SimpleResult.class,
            new UnitOfMeasurement("", ""),
            Arrays.asList(observation)
        );
    }
}
