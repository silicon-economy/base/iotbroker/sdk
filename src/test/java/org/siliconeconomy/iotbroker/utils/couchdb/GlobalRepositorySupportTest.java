/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.couchdb;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.ektorp.ViewQuery;
import org.ektorp.http.HttpClient;
import org.ektorp.support.DesignDocument;
import org.ektorp.support.View;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.tuple;
import static org.mockito.Mockito.*;

/**
 * Unit test for {@link GlobalRepositorySupport}
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
class GlobalRepositorySupportTest {
    /**
     * Test repository using {@link TestEntity}.
     */
    private static class EmptyTestRepository extends GlobalRepositorySupport<TestEntity> {
        protected EmptyTestRepository(PartitionedCouchDbConnector db, String designDocName,
                                      boolean createIfNotExists, ObjectMapper viewQueryObjectMapper) {
            super(TestEntity.class, db, designDocName, createIfNotExists, viewQueryObjectMapper);
        }
    }

    private static class TestRepositoryWithView extends GlobalRepositorySupport<TestEntity> {
        protected TestRepositoryWithView(PartitionedCouchDbConnector db, String designDocName,
                                         boolean createIfNotExists, ObjectMapper viewQueryObjectMapper) {
            super(TestEntity.class, db, designDocName, createIfNotExists, viewQueryObjectMapper);
        }

        @Global
        @View(name = "testView1", map = "mapFunction1")
        public List<TestEntity> getTestView1() {
            return List.of(new TestEntity());
        }
    }

    private final static String TEST_DB_NAME = "testdb";
    private final static String TEST_DB_PATH = "/" + TEST_DB_NAME + "/";
    private PartitionedCouchDbConnector db;

    @BeforeEach
    void setUp() {
        HttpClient client = mock(HttpClient.class);
        db = mock(PartitionedCouchDbConnector.class);

        // set up base mocks
        when(db.getConnection()).thenReturn(client);
        when(db.path()).thenReturn(TEST_DB_PATH);
    }

    @Test
    void initStandardDesignDocument_forceUpdateDesignDoc() {
        var repository = new TestRepositoryWithView(db, "testDoc", false, null);
        var currentDesignDocInDb = repository.getDesignDocumentFactory().generateGlobalFrom(repository);
        var currentDesignDocInDbId = "_design/testDoc_global";

        // Arrange
        // Modify the currentDesignDocInDb to force the code path to update the design document.
        currentDesignDocInDb.setId(currentDesignDocInDbId);
        // Remove the view defined in the repository to emulate that it currently doesn't exist in
        // the design document in the database.
        currentDesignDocInDb.removeView("testView1");
        // Pretend there is a different view existing in the database.
        currentDesignDocInDb.addView("existingView", new DesignDocument.View());
        when(db.contains(currentDesignDocInDbId)).thenReturn(true);
        when(db.get(any(), eq(currentDesignDocInDbId))).thenReturn(currentDesignDocInDb);

        // Act
        repository.initGlobalDesignDocument(repository);

        // Verify
        ArgumentCaptor<DesignDocument> designDocCaptor = ArgumentCaptor.forClass(DesignDocument.class);
        verify(db, times(1)).update(designDocCaptor.capture());
        assertThat(designDocCaptor.getAllValues())
            .extracting(
                DesignDocument::getId,
                e -> e.getAnonymous().getOrDefault("options", new HashMap<String, Object>()),
                DesignDocument::getViews
            )
            .containsExactly(
                tuple(
                    currentDesignDocInDbId,
                    Map.of("partitioned", false),
                    Map.of(
                        "existingView", new DesignDocument.View(),
                        "testView1", new DesignDocument.View("mapFunction1")
                    )
                )
            );
    }

    @Test
    void initStandardDesignDocument_createNewDesignDoc() {
        var repository = new TestRepositoryWithView(db, "testDoc", false, null);
        var globalDesignDocId = "_design/testDoc_global";

        // Arrange
        when(db.contains(globalDesignDocId)).thenReturn(false);

        // Act
        repository.initGlobalDesignDocument(repository);

        // Verify
        ArgumentCaptor<DesignDocument> designDocCaptor = ArgumentCaptor.forClass(DesignDocument.class);
        verify(db, times(1)).update(designDocCaptor.capture());
        assertThat(designDocCaptor.getAllValues())
            .extracting(
                DesignDocument::getId,
                e -> e.getAnonymous().getOrDefault("options", new HashMap<String, Object>()),
                DesignDocument::getViews
            )
            .containsExactly(
                tuple(
                    globalDesignDocId,
                    Map.of("partitioned", false),
                    Map.of("testView1", new DesignDocument.View("mapFunction1"))
                )
            );
    }

    @Test
    void initStandardDesignDocument_ensureNoEmptyDesignDocIsCreated() {
        var repository = new EmptyTestRepository(db, "testDoc", false, null);
        var globalDesignDocId = "_design/testDoc_global";

        // Arrange
        when(db.contains(globalDesignDocId)).thenReturn(false);

        // Act
        repository.initGlobalDesignDocument(repository);

        // Verify
        ArgumentCaptor<DesignDocument> designDocCaptor = ArgumentCaptor.forClass(DesignDocument.class);
        verify(db, times(0)).update(designDocCaptor.capture());
    }

    @Test
    void mergeOptions() {
        // Arrange
        var designDocDefault = new DesignDocument();
        var designDocumentWithOptions = new DesignDocument();
        var nonDefaultOptions = new HashMap<String, Object>();
        nonDefaultOptions.put("option", "invalid");
        designDocumentWithOptions.setAnonymous("options", nonDefaultOptions);

        // Act & Assert
        assertThat(GlobalRepositorySupport.mergeOptions(designDocDefault,
            designDocDefault)).isFalse();

        assertThat(GlobalRepositorySupport.mergeOptions(designDocumentWithOptions,
            designDocDefault)).isTrue();
        assertThat(designDocDefault)
            .extracting(DesignDocument::getAnonymous)
            .extracting(e -> e.getOrDefault("options", ""))
            .isEqualTo(Map.of("option", "invalid"));
    }

    @Test
    void createQuery() {
        // Arrange
        var ddocName = "testDoc";
        var repository = new TestRepositoryWithView(db, ddocName, false, null);

        // Act
        var query = repository.createQuery("testView");

        // Assert
        assertThat(query)
            .extracting(
                ViewQuery::getDesignDocId,
                ViewQuery::getDbPath,
                ViewQuery::getViewName,
                ViewQuery::getLimit
            )
            .contains(
                "_design/testDoc_global",
                TEST_DB_PATH,
                "testView",
                -1 // default
            );
    }
}
