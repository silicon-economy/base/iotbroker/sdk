/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.couchdb;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.ektorp.ComplexKey;
import org.ektorp.ViewQuery;
import org.ektorp.http.HttpClient;
import org.ektorp.support.DesignDocument;
import org.ektorp.support.View;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Test cases for {@link PartitionedRepositorySupport}
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
class PartitionedRepositorySupportTest {

    /**
     * Test repository using {@link TestEntity}.
     */
    private static class TestRepository extends PartitionedRepositorySupport<TestEntity> {
        protected TestRepository(PartitionedCouchDbConnector db, String designDocName,
                                 boolean createIfNotExists, ObjectMapper viewQueryObjectMapper) {
            super(TestEntity.class, db, designDocName, createIfNotExists, viewQueryObjectMapper);
        }

        @Global
        @View(name = "testView1", map = "mapFunction1")
        public List<TestEntity> getTestView1() {
            return List.of(new TestEntity());
        }
    }

    private final static String TEST_DB_NAME = "testdb";
    private final static String TEST_DB_PATH = "/" + TEST_DB_NAME + "/";
    private HttpClient client;
    private PartitionedCouchDbConnector db;

    @BeforeEach
    void setUp() {
        client = mock(HttpClient.class);
        db = mock(PartitionedCouchDbConnector.class);

        // set up base mocks
        when(db.getConnection()).thenReturn(client);
        when(db.path()).thenReturn(TEST_DB_PATH);
    }

    /**
     * Simple test for {@link PartitionedRepositorySupport#createQuery(String, String)}.
     */
    @Test
    void createQuery() {
        // create test repository
        var repository = new TestRepository(db, "testDoc", false, null);

        var query = repository.createQuery("testView", "testPartition");

        assertEquals(TEST_DB_PATH, query.getDbPath());
        assertEquals("_partition/testPartition/_design/testDoc", query.getDesignDocId());
    }

    @Test
    void queryPartitionedView() {
        // Arrange
        // create test repository
        var repository = new TestRepository(db, "testDoc", false, null);

        // Act
        repository.queryPartitionedView("testView", "testPartition");

        // Assert & Verify
        ArgumentCaptor<ViewQuery> viewQueryCaptor = ArgumentCaptor.forClass(ViewQuery.class);
        verify(db).queryView(viewQueryCaptor.capture(), eq(TestEntity.class));
        assertThat(viewQueryCaptor.getValue())
            .extracting(
                ViewQuery::getDesignDocId,
                ViewQuery::getViewName,
                ViewQuery::isIncludeDocs
            )
            .contains(
                "_partition/testPartition/_design/testDoc",
                "testView",
                true
            );
    }

    @Test
    void queryPartitionedView_WithStringKey() {
        // Arrange
        // create test repository
        var repository = new TestRepository(db, "testDoc", false, null);

        // Act
        repository.queryPartitionedView("testView", "testPartition", "testKey");

        // Assert & Verify
        ArgumentCaptor<ViewQuery> viewQueryCaptor = ArgumentCaptor.forClass(ViewQuery.class);
        verify(db).queryView(viewQueryCaptor.capture(), eq(TestEntity.class));
        assertThat(viewQueryCaptor.getValue())
            .extracting(
                ViewQuery::getDesignDocId,
                ViewQuery::getViewName,
                ViewQuery::getKey,
                ViewQuery::isIncludeDocs
            )
            .contains(
                "_partition/testPartition/_design/testDoc",
                "testView",
                "testKey",
                true
            );
    }

    @Test
    void queryPartitionedView_WithIntegerKey() {
        // Arrange
        // create test repository
        var repository = new TestRepository(db, "testDoc", false, null);

        // Act
        repository.queryPartitionedView("testView", "testPartition", 42);

        // Assert & Verify
        ArgumentCaptor<ViewQuery> viewQueryCaptor = ArgumentCaptor.forClass(ViewQuery.class);
        verify(db).queryView(viewQueryCaptor.capture(), eq(TestEntity.class));
        assertThat(viewQueryCaptor.getValue())
            .extracting(
                ViewQuery::getDesignDocId,
                ViewQuery::getViewName,
                ViewQuery::getKey,
                ViewQuery::isIncludeDocs
            )
            .contains(
                "_partition/testPartition/_design/testDoc",
                "testView",
                42,
                true
            );
    }

    @Test
    void queryPartitionedView_WithComplexKey() {
        // Arrange
        // create test repository
        var repository = new TestRepository(db, "testDoc", false, null);
        ComplexKey complexKey = ComplexKey.of("complex-component");

        // Act
        repository.queryPartitionedView("testView", "testPartition", complexKey);

        // Assert & Verify
        ArgumentCaptor<ViewQuery> viewQueryCaptor = ArgumentCaptor.forClass(ViewQuery.class);
        verify(db).queryView(viewQueryCaptor.capture(), eq(TestEntity.class));
        assertThat(viewQueryCaptor.getValue())
            .extracting(
                ViewQuery::getDesignDocId,
                ViewQuery::getViewName,
                ViewQuery::getKey,
                ViewQuery::isIncludeDocs
            )
            .contains(
                "_partition/testPartition/_design/testDoc",
                "testView",
                complexKey,
                true
            );
    }

    /**
     * The {@code queryView*} test methods ensure that the global design document is queried for
     * unpartitioned queries.
     */
    @Test
    void queryView() {
        // Arrange
        var repository = new TestRepository(db, "testDoc", false, null);

        // Act
        repository.queryView("testView");

        // Assert & Verify
        ArgumentCaptor<ViewQuery> viewQueryCaptor = ArgumentCaptor.forClass(ViewQuery.class);
        verify(db).queryView(viewQueryCaptor.capture(), eq(TestEntity.class));
        assertThat(viewQueryCaptor.getValue())
            .extracting(
                ViewQuery::getDesignDocId,
                ViewQuery::getViewName,
                ViewQuery::isIncludeDocs
            )
            .contains(
                "_design/testDoc_global",
                "testView",
                true
            );
    }

    @Test
    void queryView_WithStringKey() {
        // Arrange
        var repository = new TestRepository(db, "testDoc", false, null);

        // Act
        repository.queryView("testView", "testKey");

        // Assert & Verify
        ArgumentCaptor<ViewQuery> viewQueryCaptor = ArgumentCaptor.forClass(ViewQuery.class);
        verify(db).queryView(viewQueryCaptor.capture(), eq(TestEntity.class));
        assertThat(viewQueryCaptor.getValue())
            .extracting(
                ViewQuery::getDesignDocId,
                ViewQuery::getViewName,
                ViewQuery::getKey,
                ViewQuery::isIncludeDocs
            )
            .contains(
                "_design/testDoc_global",
                "testView",
                "testKey",
                true
            );
    }

    @Test
    void queryView_WithIntegerKey() {
        // Arrange
        var repository = new TestRepository(db, "testDoc", false, null);

        // Act
        repository.queryView("testView", 42);

        // Assert & Verify
        ArgumentCaptor<ViewQuery> viewQueryCaptor = ArgumentCaptor.forClass(ViewQuery.class);
        verify(db).queryView(viewQueryCaptor.capture(), eq(TestEntity.class));
        assertThat(viewQueryCaptor.getValue())
            .extracting(
                ViewQuery::getDesignDocId,
                ViewQuery::getViewName,
                ViewQuery::getKey,
                ViewQuery::isIncludeDocs
            )
            .contains(
                "_design/testDoc_global",
                "testView",
                42,
                true
            );
    }

    @Test
    void queryView_WithComplexKey() {
        // Arrange
        var repository = new TestRepository(db, "testDoc", false, null);
        ComplexKey complexKey = ComplexKey.of("complex-component");

        // Act
        repository.queryView("testView", complexKey);

        // Assert & Verify
        ArgumentCaptor<ViewQuery> viewQueryCaptor = ArgumentCaptor.forClass(ViewQuery.class);
        verify(db).queryView(viewQueryCaptor.capture(), eq(TestEntity.class));
        assertThat(viewQueryCaptor.getValue())
            .extracting(
                ViewQuery::getDesignDocId,
                ViewQuery::getViewName,
                ViewQuery::getKey,
                ViewQuery::isIncludeDocs
            )
            .contains(
                "_design/testDoc_global",
                "testView",
                complexKey,
                true
            );
    }

    @Test
    void shouldCreatePartitionedAndGlobalDesignDocumentWithViews() {
        var repository = new TestRepository(db, "testDoc", false, null);

        // Arrange
        when(db.contains(anyString())).thenReturn(false);

        // Act
        repository.initStandardDesignDocument();

        // Verify
        ArgumentCaptor<DesignDocument> designDocCaptor = ArgumentCaptor.forClass(DesignDocument.class);
        verify(db, times(2)).update(designDocCaptor.capture());
        assertThat(designDocCaptor.getAllValues())
            .extracting(
                DesignDocument::getId,
                DesignDocument::getViews
            )
            .containsExactlyInAnyOrder(
                tuple(
                    "_design/testDoc",
                    Map.of("testView1", new DesignDocument.View("mapFunction1"))
                ),
                tuple(
                    "_design/testDoc_global",
                    Map.of("testView1", new DesignDocument.View("mapFunction1"))
                )
            );
    }
}
