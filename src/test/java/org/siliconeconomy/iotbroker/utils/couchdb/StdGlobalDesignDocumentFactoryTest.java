/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.couchdb;

import org.ektorp.support.DesignDocument;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Unit test for {@link StdGlobalDesignDocumentFactory}
 *
 * @author D. Ronnenberg
 */
class StdGlobalDesignDocumentFactoryTest {
    StdGlobalDesignDocumentFactory factory;

    @BeforeEach
    void setUp() {
        factory = new StdGlobalDesignDocumentFactory();
    }

    @Test
    void testPartitionedViewGeneration() {
        DesignDocument ddoc =
            factory.generateFrom(new GlobalViewGeneratorTest.TestRepoGlobalViews());

        // Important!
        var anonymousAttributes = ddoc.getAnonymous();

        assertThat(anonymousAttributes).isEmpty();

        // We create global views always in both design documents.
        assertThat(ddoc.getViews()).hasSize(3);
        assertThat(ddoc.getViews()).containsOnlyKeys("view_1", "view_2", "reduce_1");
        assertThat(ddoc.getLists()).isEmpty();
        assertThat(ddoc.getShows()).isEmpty();
        assertThat(ddoc.getUpdates()).isEmpty();
    }

    @Test
    void testGlobalViewGeneration() {
        // Test if we get the global views.
        DesignDocument ddoc =
            factory.generateGlobalFrom(new GlobalViewGeneratorTest.TestRepoGlobalViews());

        assertThat(ddoc.getViews()).hasSize(3);
        assertThat(ddoc.getViews()).containsOnlyKeys("view_1", "view_2", "reduce_1");

        // There should be options for this design document not to be partitioned.
        var anonymousAttributes = ddoc.getAnonymous();
        assertThat(anonymousAttributes).containsKey("options");
        Map<String, Object> options = (Map<String, Object>) anonymousAttributes.get("options");
        assertThat(options).isNotNull().containsKey("partitioned");

        boolean partitioned = (boolean) options.get("partitioned");
        assertThat(partitioned).isFalse();
    }
}
