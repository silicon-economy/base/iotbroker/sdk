/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.couchdb;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.ektorp.BulkDeleteDocument;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.support.View;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.net.MalformedURLException;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration test for testing {@link PartitionedRepositorySupport}
 * <p>
 * This test will connect to a live CouchDB instance and create a new database with the name
 * {@link #TEST_DB_NAME}. If the database already exists all its documents will be deleted. Make
 * sure to set {@link #URL}, {@link #USERNAME} and {@link #PASSWORD} to the correct values
 * matching your CouchDB instance.
 *
 * @author D. Ronnenberg
 */
@Disabled
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PartitionedRepositoryTest {
    private final static String URL = "http://localhost:5984";
    private final static String USERNAME = "guest";
    private final static String PASSWORD = "guest";
    private final static String TEST_DB_NAME = "testdb";

    private PartitionedCouchDbConnector db;
    private TestRepository repository;

    @BeforeAll
    void setUp() throws MalformedURLException {
        HttpClient client = new StdHttpClient.Builder()
            .url(URL)
            .username(USERNAME)
            .password(PASSWORD)
            .caching(false)
            .build();

        var instance = new StdPartitionedCouchDbInstance(client);

        db = new StdPartitionedCouchDbConnector(TEST_DB_NAME, instance);
        repository = new TestRepository(db, "testEntity", true, new ObjectMapper());
    }

    @Test
    void startTest() {
        // Delete all documents in test db.
        List<BulkDeleteDocument> deleteAllDocuments =
            repository.getAll().stream().map(BulkDeleteDocument::of).collect(Collectors.toList());

        assert (db.executeBulk(deleteAllDocuments).isEmpty());

        // Create some test entities.
        List<TestEntity> testEntities = List.of(
            new TestEntity("partition1:0", null, "payload0"),
            new TestEntity("partition1:1", null, "payload1"),
            new TestEntity("partition1:2", null, "payload1"),
            new TestEntity("partition2:0", null, "payload0"),
            new TestEntity("partition2:1", null, "payload1"),
            new TestEntity("partition2:2", null, "payload1")
        );

        // Store test Documents.
        for (var entity : testEntities) repository.add(entity);

        // Perform a partitioned query on a view.
        var queried = repository.queryPartitioned("partition1", "payload1");

        assertThat(queried).hasSize(2);

        // Perform a normal query on a view.
        queried = repository.normalQuery("payload1");

        assertThat(queried).hasSize(4);
    }

    /**
     * Test repository using {@link TestEntity}.
     */
    private class TestRepository extends PartitionedRepositorySupport<TestEntity> {
        protected TestRepository(PartitionedCouchDbConnector db, String designDocName,
                                 boolean createIfNotExists, ObjectMapper viewQueryObjectMapper) {
            super(TestEntity.class, db, designDocName, createIfNotExists, viewQueryObjectMapper);

            // This line is important! Without it no design documents and therefore views will be
            // created!
            initStandardDesignDocument();
        }

        @View(name = "byPayloadPartitioned", map = "function(doc) { emit(doc.payload, null)}")
        List<TestEntity> queryPartitioned(String partition, String payload) {
            var query = createQuery("byPayloadPartitioned", partition)
                .includeDocs(true)
                .startKey(payload);

            return db.queryView(query, TestEntity.class);
        }

        @Global
        @View(name = "byPayload", map = "function(doc) { emit( doc.payload, null )}")
        List<TestEntity> normalQuery(String payload) {
            var query = createQuery("byPayload")
                .includeDocs(true)
                .startKey(payload);

            return db.queryView(query, TestEntity.class);
        }
    }

}
