/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.couchdb;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * Entity for testing.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
class TestEntity {
    /**
     * The unique ID of the entity in the database.
     */
    @JsonProperty("_id")
    private String id;
    /**
     * The revision of the entity in the database.
     */
    @JsonProperty("_rev")
    private String revision;

    private String payload;
}
