/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.couchdb;

import org.apache.commons.io.IOUtils;
import org.ektorp.DbAccessException;
import org.ektorp.DbPath;
import org.ektorp.http.HttpClient;
import org.ektorp.http.HttpResponse;
import org.ektorp.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test cases for {@link StdPartitionedCouchDbInstance}.
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
class StdPartitionedCouchDbInstanceTest {

    /**
     * Class under test.
     */
    private StdPartitionedCouchDbInstance couchDbInstance;
    /**
     * Test dependencies.
     */
    private HttpClient httpClient;
    /**
     * Test environment.
     */
    private final static String TEST_DB_NAME = "testdb";
    private final static String TEST_DB_PATH = "/" + TEST_DB_NAME + "/";

    @BeforeEach
    void setUp() {
        httpClient = mock(HttpClient.class);
        couchDbInstance = new StdPartitionedCouchDbInstance(httpClient);
    }

    @Test
    void checkIfDbExists_DbExists() {
        // Arrange
        var response = mock(HttpResponse.class);
        when(httpClient.get(anyString())).thenReturn(response);
        when(response.getContent())
            .thenReturn(IOUtils.toInputStream("{\"props\":{\"partitioned\": true}}"));
        when(response.isSuccessful()).thenReturn(true);

        // Act
        boolean result = couchDbInstance.checkIfDbExists(TEST_DB_PATH);

        // Assert
        assertTrue(result);
    }

    @Test
    void checkIfDbExists_DbExistsButNotPartitioned() {
        // Arrange
        var response = mock(HttpResponse.class);
        when(httpClient.get(anyString())).thenReturn(response);
        when(response.isSuccessful()).thenReturn(true);
        var dbPath = DbPath.fromString(TEST_DB_PATH);

        // Act & Assert (partition property not set)
        when(response.getContent()).thenReturn(IOUtils.toInputStream("{\"props\":{}}"));
        assertThatThrownBy(() -> couchDbInstance.checkIfDbExists(dbPath))
            .isInstanceOf(DbAccessException.class)
            .hasMessageContaining(TEST_DB_NAME);

        // Act & Assert (properties missing)
        when(response.getContent()).thenReturn(IOUtils.toInputStream("{}"));
        assertThatThrownBy(() -> couchDbInstance.checkIfDbExists(dbPath))
            .isInstanceOf(DbAccessException.class)
            .hasMessageContaining(TEST_DB_NAME);
    }

    @Test
    void checkIfDbExists_DbDoesNotExist() throws IOException {
        // Arrange
        var response = mock(HttpResponse.class);
        when(httpClient.get(anyString())).thenReturn(response);
        when(response.getCode()).thenReturn(HttpStatus.NOT_FOUND);
        when(response.isSuccessful()).thenReturn(false);

        // Act
        boolean result = couchDbInstance.checkIfDbExists(TEST_DB_PATH);

        // Assert
        assertFalse(result);
    }

    /**
     * Test {@link StdPartitionedCouchDbInstance#createDatabaseIfNotExists(DbPath)} for the
     * case that no database exists on that path and creation is successful.
     */
    @Test
    void createDatabaseIfNotExists_CreationSuccessful() {
        // Arrange
        // response for the HTTP get in checkIfDbExists()
        var getResponse = mock(HttpResponse.class);
        when(httpClient.get(anyString())).thenReturn(getResponse);
        when(getResponse.getCode()).thenReturn(HttpStatus.NOT_FOUND);
        when(getResponse.isSuccessful()).thenReturn(false);

        // response for the HTTP put
        var putResponse = mock(HttpResponse.class);
        when(httpClient.put(anyString())).thenReturn(putResponse);
        when(putResponse.getContent()).thenReturn(IOUtils.toInputStream("{\"ok\": true}"));
        when(putResponse.isSuccessful()).thenReturn(true);

        // Act
        boolean result = couchDbInstance.createDatabaseIfNotExists(TEST_DB_PATH);

        // Assert
        assertTrue(result);
    }

    /**
     * Test {@link StdPartitionedCouchDbInstance#createDatabaseIfNotExists(DbPath)} for the
     * case that no database exists on that path and creation is unsuccessful.
     */
    @Test
    void createDatabaseIfNotExists_CreationFailed() {
        // Assert
        // response for the HTTP get in checkIfDbExists()
        var getResponse = mock(HttpResponse.class);
        when(httpClient.get(anyString())).thenReturn(getResponse);
        when(getResponse.getCode()).thenReturn(HttpStatus.NOT_FOUND);
        when(getResponse.isSuccessful()).thenReturn(false);

        // response for the unsuccessful HTTP put
        var putResponse = mock(HttpResponse.class);
        when(httpClient.put(anyString())).thenReturn(putResponse);
        when(putResponse.getCode()).thenReturn(HttpStatus.NOT_FOUND);
        when(putResponse.getContent())
            .thenReturn(IOUtils.toInputStream("{\"error\": \"test threw error!\"}"));
        when(putResponse.getRequestURI()).thenReturn("testURI");
        when(putResponse.isSuccessful()).thenReturn(false);

        // Act & Assert
        assertThatThrownBy(() -> couchDbInstance.createDatabaseIfNotExists(TEST_DB_PATH))
            .isInstanceOf(DbAccessException.class)
            .hasMessageContaining("test threw error!");
    }
}
