/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.couchdb;

import org.ektorp.support.View;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Unit test for {@link GlobalViewGenerator}
 *
 * @author D. Ronnenberg
 */
class GlobalViewGeneratorTest {

    GlobalViewGenerator generator;

    @BeforeEach
    void setUp() {
        generator = new GlobalViewGenerator();
    }

    @Test
    void testNormalViewsOnly() {
        var repo = new TestRepoNormalViews();
        var views = generator.generateViews(repo);
        var globalViews = generator.generateGlobalViews(repo);

        assertThat(views)
            .hasSize(3)
            .containsOnlyKeys("view_1", "view_2", "reduce_1");
        assertThat(globalViews).isEmpty();
    }

    @Test
    void testPartitionedViewsOnly() {
        var repo = new TestRepoGlobalViews();
        var views = generator.generateViews(repo);
        var globalViews = generator.generateGlobalViews(repo);

        assertThat(views)
            .hasSize(3)
            .containsOnlyKeys("view_1", "view_2", "reduce_1");
        assertThat(globalViews)
            .hasSize(3)
            .containsOnlyKeys("view_1", "view_2", "reduce_1");
    }

    @Test
    void testMixedViews() {
        var repo = new TestRepoMixedViews();
        var views = generator.generateViews(repo);
        var globalViews = generator.generateGlobalViews(repo);

        assertThat(views)
            .hasSize(6)
            .containsOnlyKeys("view_1", "view_2", "reduce_1", "view_3", "view_4", "reduce_2");
        assertThat(globalViews)
            .hasSize(3)
            .containsOnlyKeys("view_3", "view_4", "reduce_2");
    }

    @Test
    void testLoadPartitionedFromFile() {
        var globalViews = generator.generateGlobalViews(new TestRepoLoadFromFile());

        assertThat(globalViews)
            .hasSize(1)
            .containsOnlyKeys("test")
            .allSatisfy((key, view) -> {
                assertThat(view.getMap()).isEqualTo("testMap");
                assertThat(view.getReduce()).isEqualTo("testReduce");
            });
    }

    public static class TestRepoNormalViews {
        @View(name = "view_1", map = "this is a test")
        public List<TestEntity> view_1() {
            return List.of(new TestEntity());
        }

        @View(name = "view_2", map = "this is a test")
        public List<TestEntity> view_2() {
            return List.of(new TestEntity());
        }

        @View(name = "reduce_1", reduce = "this is a test")
        public List<TestEntity> reduce_1() {
            return List.of(new TestEntity());
        }
    }

    public static class TestRepoGlobalViews {
        @Global
        @View(name = "view_1", map = "this is a test")
        public List<TestEntity> view_1() {
            return List.of(new TestEntity());
        }

        @Global
        @View(name = "view_2", map = "this is a test")
        public List<TestEntity> view_2() {
            return List.of(new TestEntity());
        }

        @Global
        @View(name = "reduce_1", reduce = "this is a test")
        public List<TestEntity> reduce_1() {
            return List.of(new TestEntity());
        }
    }

    public static class TestRepoMixedViews {
        @View(name = "view_1", map = "this is a test")
        public List<TestEntity> view_1() {
            return List.of(new TestEntity());
        }

        @View(name = "view_2", map = "this is a test")
        public List<TestEntity> view_2() {
            return List.of(new TestEntity());
        }

        @View(name = "reduce_1", reduce = "this is a test")
        public List<TestEntity> reduce_1() {
            return List.of(new TestEntity());
        }

        @Global
        @View(name = "view_3", map = "this is a test")
        public List<TestEntity> view_3() {
            return List.of(new TestEntity());
        }

        @Global
        @View(name = "view_4", map = "this is a test")
        public List<TestEntity> view_4() {
            return List.of(new TestEntity());
        }

        @Global
        @View(name = "reduce_2", reduce = "this is a test")
        public List<TestEntity> reduce_2() {
            return List.of(new TestEntity());
        }
    }

    public static class TestRepoLoadFromFile {
        @Global
        @View(name = "test", file = "testrepo-view.json")
        public List<TestEntity> someName() {
            return List.of(new TestEntity());
        }
    }
}
