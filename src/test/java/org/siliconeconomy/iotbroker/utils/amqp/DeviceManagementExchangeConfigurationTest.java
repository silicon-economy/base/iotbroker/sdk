/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.amqp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test cases for {@link DeviceManagementExchangeConfiguration}.
 *
 * @author M. Grzenia
 */
class DeviceManagementExchangeConfigurationTest {

    @Test
    void formatDeviceInstanceUpdateRoutingKey() {
        assertEquals(
            "deviceInstance.myDeviceTypeIdentifier.update",
            DeviceManagementExchangeConfiguration
                .formatDeviceInstanceUpdateRoutingKey("myDeviceTypeIdentifier")
        );
    }

    @Test
    void formatDeviceTypeUpdateRoutingKey() {
        assertEquals(
            "deviceType.myDeviceTypeIdentifier.update",
            DeviceManagementExchangeConfiguration
                .formatDeviceTypeUpdateRoutingKey("myDeviceTypeIdentifier")
        );
    }
}
