/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.amqp;

import com.rabbitmq.client.BuiltinExchangeType;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test cases for the various exchange configurations.
 *
 * @author M. Grzenia
 */
class ExchangeConfigurationTest {

    @Test
    void testExchangeTypes() {
        assertThat(SensorDataExchangeConfiguration.TYPE).isEqualTo(BuiltinExchangeType.TOPIC.getType());
    }
}
