/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.distribution;

import com.rabbitmq.client.DeliverCallback;
import org.siliconeconomy.iotbroker.utils.amqpclient.AmqpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.concurrent.TimeoutException;

/**
 * Helper class for adapter instances that should be scaled
 */
public class DistributionHelper implements Runnable{

    private static final Logger LOGGER = LoggerFactory.getLogger(DistributionHelper.class);
    /**
     * Parameters for amqp
     */
    private static final String DISTRIBUTION_TASK_QUEUE = "task_queue";
    private static final String DISTRIBUTION_INFO_EXCHANGE = "info";
    private final HashSet<String> ids = new HashSet<>();
    /**
     * The taskClient is for sending IDs to the adapter instances
     */
    private AmqpClient taskClient;
    /**
     * The infoClient is for receiving signals to redistribute
     */
    private AmqpClient infoClient;
    /**
     * This variable is to notify the PropertyChangeListener (the adapter instance) if the IDs have changed
     */
    private final PropertyChangeSupport idUpdates = new PropertyChangeSupport(this);

    /**
     * Adds the instance to the distribution by connecting to the amqp exchanges
     */
    @Override
    public void run() {
        LOGGER.info("Starting DistributionHelper");
        connectToAmqp();
    }

    /**
     * This method connects the taskClient and infoClient to amqp and starts waiting for messages
     */
    public void connectToAmqp() {
        try {
            taskClient = new AmqpClient();
            taskClient.connect(true);
            infoClient = new AmqpClient();
            infoClient.connect(true);
            infoClient.declareFanoutExchange(DISTRIBUTION_INFO_EXCHANGE);
            LOGGER.info("info & task client successfully connected");
            waitForTasks();
            waitForInfoMessages();
        } catch (IOException | TimeoutException e) {
            LOGGER.error("Error occurred while connecting to amqp ", e);
        }
    }

    /**
     * Waits for ids
     * If a new id is received, it will be pushed to the current ids and PropertyChangeListener will be notified
     */
    public void waitForTasks() {
        LOGGER.info("Waiting for messages");
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            LOGGER.info(" [x] Received {}", message);
            this.ids.add(message);
            idUpdates.firePropertyChange("new_id", null, message);
        };

        try {
            taskClient.bindConsume("", DISTRIBUTION_TASK_QUEUE, "", true, deliverCallback);
        } catch (IOException e) {
            LOGGER.error("Error occurred while binding to the task exchange ", e);
        }
    }

    /**
     * Waits for info messages
     * If a delete message is received, the IDs are deleted
     */
    public void waitForInfoMessages() {
        LOGGER.info("Waiting for info messages");
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            LOGGER.info(" [x] Received info: {}", message);
            // 000 is the delete message
            if (message.equals("000")) {
                LOGGER.info("Delete message received");
                this.clearIds();
            }
        };
        try {
            infoClient.bindConsume(DISTRIBUTION_INFO_EXCHANGE, "", true, deliverCallback);
        } catch (IOException e) {
            LOGGER.error("IOException occurred while waiting for info messages ", e);
        }
    }

    /**
     * This method clears the current IDs and notifies the PropertyChangeListener
     */
    public void clearIds() {
        LOGGER.info("clearing ids");
        this.ids.clear();
        idUpdates.firePropertyChange("deleted_ids", null, this.ids);
    }

    /**
     * This method returns the current IDs
     *
     * @return current IDs
     */
    public HashSet<String> getIds() {
        return this.ids;
    }

    /**
     * This method adds the listener to idUpdates
     * idUpdates informs the listener about changes of the IDs
     *
     * @param listener listener that wants to subscribe to idUpdates
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        idUpdates.addPropertyChangeListener(listener);
    }

    /**
     * This method deletes a listener from idUpdates
     *
     * @param listener listener that wants to unsubscribe
     */
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        idUpdates.removePropertyChangeListener(listener);
    }

}

