/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.distribution;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

/**
 * class for testing the DistributionHelper
 */
public class TestAdapter implements Observer {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestAdapter.class);
    private DistributionHelper distributionHelper;

    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        TestAdapter adapter = new TestAdapter();
        adapter.start();
    }

    public void start() throws TimeoutException, IOException {
        distributionHelper = new DistributionHelper();
        Thread tr = new Thread(distributionHelper);
        tr.start();

        Scanner sc = new Scanner(System.in);

        boolean end = false;

        while(!end){
            LOGGER.info("1) Ids\n2) end");
            int choice = sc.nextInt();
            if(choice == 1 ){
                String ids = distributionHelper.getIds().toString();
                LOGGER.info(ids);
            }
            else if(choice == 2){
                end = true;
            }
        }
    }

    @Override
    public void update(Observable observable, Object obj){
        LOGGER.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        String ids = distributionHelper.getIds().toString();
        LOGGER.info("IDs changed!\n new Ids: {}", ids );
        LOGGER.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    }

}
