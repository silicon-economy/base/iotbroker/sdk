/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.amqpclient;

import com.rabbitmq.client.*;
import org.siliconeconomy.iotbroker.utils.parameter.CommonParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * AmqpClient to connect to amqp broker.
 * Wraps functionality of rabbitmq amqp client.
 *
 * @author smengers
 * @version 123
 */
public class AmqpClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(AmqpClient.class);
    private static final long RECONNECT_INTERVAL = 10000;
    private Channel channel;
    private Connection connection;
    private boolean reconnect;

    /**
     * Connects to AMQPBroker with  host and port defined in environment variables.
     * If reconnect = true retries until connection is made.
     * If reconnect = false one try to connect on fail exception will be thrown.
     * Reconnects on connection lost, if connection was established.
     *
     * @param reconnect If true method will block thread until connection is established
     * @throws IOException      Thrown if timeout error occurs while connecting
     * @throws TimeoutException Thrown if timeout error occurs while connecting
     */
    public void connect(Boolean reconnect) throws IOException, TimeoutException {
        this.reconnect = reconnect;
        int port = Integer.parseInt(CommonParameters.RABBIT_PORT);
        String host = CommonParameters.RABBIT_HOST;
        if (this.reconnect) {
            try {
                connect(host, port);
            } catch (IOException | TimeoutException e) {
                LOGGER.warn("Could not connect. retrying in {} milliseconds.", RECONNECT_INTERVAL, e);
                try {
                    Thread.sleep(RECONNECT_INTERVAL);
                } catch (InterruptedException exc) {
                    LOGGER.debug("Interrupted!", exc);
                    // Restore interrupted state of the running thread
                    Thread.currentThread().interrupt();
                }
                connect(true);
            }
        } else {
            connect(CommonParameters.RABBIT_HOST, port);
        }
    }

    /**
     * closes connection
     *
     * @throws IOException
     * @throws TimeoutException
     */
    public void closeConnection() throws IOException, TimeoutException {
        if (this.connection.isOpen()) {
            this.channel.close();
            connection.close();
        }
    }

    /**
     * Connects to AMQP broker with given host and port.
     * Reconnects on connection lost, if connection was established.
     *
     * @param host host of amqp broker
     * @param port port of amqp broker
     * @throws IOException      Thrown if IO error occurs while connecting
     * @throws TimeoutException Thrown if timeout error occurs while connecting
     */
    public void connect(String host, int port) throws IOException, TimeoutException {
        String username = CommonParameters.RABBITMQ_USERNAME;
        String password = CommonParameters.RABBITMQ_PASSWORD;
        connect(host, port, username, password);
    }

    /**
     * Connects to AMQP broker with given host and port with given password and username
     * Reconnects on connection lost, if connection was established.
     *
     * @param host     host of amqp broker
     * @param port     port of amqp broker
     * @param username username to connect (if null no username will be set)
     * @param password password to connect (if null no password will be set)
     * @throws IOException      Thrown if IO error occurs while connecting
     * @throws TimeoutException Thrown if timeout error occurs while connecting
     */
    public void connect(String host, int port, String username, String password) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setPort(port);
        if (username != null) {
            factory.setUsername(username);
        } else {
            LOGGER.warn("Connecting without username");
        }
        if (password != null) {
            factory.setPassword(password);
        }
        else {
            LOGGER.warn("Connecting without password");
        }

        LOGGER.info("Trying to connect to AMQP broker with: {}:{}", host, port);
        this.connection = factory.newConnection();
        channel = connection.createChannel();
        connection.addShutdownListener(e -> {
            LOGGER.info("Connection shut down. Try to restart");
            try {
                Thread.sleep(5000);
                closeConnection();
                connect(reconnect);
            } catch (InterruptedException exc) {
                LOGGER.debug("Interrupted!", exc);
                // Restore interrupted state of the running thread
                Thread.currentThread().interrupt();
            } catch (IOException | TimeoutException exc) {
                LOGGER.error("Error reconnecting to broker: ", exc);
            }
            LOGGER.info(String.format("Connection status is now %s", connection.isOpen() ? " open" : "closed"));
        });

        LOGGER.info("Successfully connected to broker");
    }

    public void declareTopicExchange(String exchangeName) throws IOException {
        channel.exchangeDeclare(exchangeName, BuiltinExchangeType.TOPIC.getType(), false);
    }

    public void declareDurableTopicExchange(String exchangeName) throws IOException {
        channel.exchangeDeclare(exchangeName, BuiltinExchangeType.TOPIC.getType(), true);
    }

    public void declareFanoutExchange(String exchangeName) throws IOException {
        channel.exchangeDeclare(exchangeName, BuiltinExchangeType.FANOUT.getType());
    }

    public void basicQos(int count) throws IOException {
        channel.basicQos(count);
    }

    /**
     * Publishes given message with given routing key to given exchange.
     * Exchange needs to be declared before.
     *
     * @param exchange  exchange to publish to.
     * @param queueName name of the queue, acts like a routing key when using direct exchange.
     * @param message   message to publish
     * @throws IOException            Thrown if IO error occurs
     * @throws AlreadyClosedException Thrown if channel is already closed
     */
    public void publish(String exchange, String queueName, String message) throws IOException {
        //TODO implement publisher confirms for maximum safety
        //https://www.rabbitmq.com/confirms.html
        channel.basicPublish(exchange, queueName, null, message.getBytes());
    }

    /**
     * Binds queue with given name to given exchange with given routingkey.
     * Exchange needs to be declared before.
     * Queue will become declared.
     * Binds callback to queue.
     *
     * @param exchange   Exchange to consume from
     * @param queueName  Queue name to bind
     * @param routingKey routing key to bind queue.
     * @param callback   callback to receive messages.
     * @param autoAck    enable autoack
     * @throws IOException Thrown if IO error occurs.
     */
    public void bindConsume(String exchange, String queueName, String routingKey, boolean autoAck, DeliverCallback callback) throws IOException {
        channel.queueDeclare(queueName, true, false, false, null);
        if (!exchange.equals("")) {
            channel.queueBind(queueName, exchange, routingKey);
        }
        channel.basicConsume(queueName, autoAck, callback, consumer -> {
        });
    }

    /**
     * Binds queue with random name to given exchange with given routingkey.
     * Exchange needs to be declared before.
     * Queue will become declared.
     * Binds callback to queue.
     *
     * @param exchange   Exchange to consume from
     * @param routingKey routing key to bind queue.
     * @param autoAck    enable autoack
     * @param callback   callback to receive messages.
     * @throws IOException Thrown if IO error occurs.
     */
    public void bindConsume(String exchange, String routingKey, boolean autoAck, DeliverCallback callback) throws IOException {
        String queueName = channel.queueDeclare().getQueue();
        if (!exchange.equals("")) {
            channel.queueBind(queueName, exchange, routingKey);
        }

        channel.basicConsume(queueName, autoAck, callback, consumer -> {
        });
    }

    /**
     * Binds given queue for given exchange to given routing key
     *
     * @param distributionExchange Exchange to bind queue.
     * @param queueName            queue to bind.
     * @param routingKey           routing key to bin queue to.
     */
    public void bindQueue(String distributionExchange, String queueName, String routingKey) throws IOException {
        channel.queueDeclare(queueName, true, false, true, null);
        channel.queueBind(queueName, distributionExchange, routingKey);
    }

    public Channel getChannel() {
        return this.channel;
    }
}
