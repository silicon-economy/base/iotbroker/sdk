/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.parameter;

/**
 * A class listing common application parameters and constants.
 * <p>
 * Those should mostly contain parameters and constants for use within this
 * library. Exceptions are common MQTT topics, database collections, and the
 * likes.
 * <p>
 * Do not define adapter specific parameters here! Instead extend this class in
 * your project and add the desired parameters in the same manner as below if
 * you want them to be defined in a single place.
 */
public class CommonParameters {


    private CommonParameters() {}

    public static final String DATA_EXCHANGE_NAME = ParameterReader.getValue("DATA_EXCHANGE_NAME", "data");
    public static final String RABBITMQ_USERNAME = ParameterReader.getValue("RABBITMQ_USERNAME", null);
    public static final String RABBITMQ_PASSWORD = ParameterReader.getValue("RABBITMQ_PASSWORD", null);
    public static final String RABBIT_HOST = ParameterReader.getValue("RABBITMQ_HOST", "localhost");
    public static final String RABBIT_PORT = ParameterReader.getValue("RABBITMQ_PORT", "5672");

    // TTN
    public static final String TTN_URI = ParameterReader.getValue("TTN_MQTT_SERVERURI", "tcp://eu.thethings.network:1883");
    public static final String TTN_CLIENT_ID = ParameterReader.getValue("TTN_CLIENT_ID", null);
    public static final String TTN_TOPIC_UP = ParameterReader.getValue("TTN_TOPIC_UP", null);
    public static final String TTN_USERNAME = ParameterReader.getValue("TTN_USERNAME", null);
    public static final String TTN_APPKEY = ParameterReader.getValue("TTN_APPKEY", null);

    // MQTT
    public static final String MQTT_HOST = ParameterReader.getValue("MQTT_BROKER_HOSTNAME", "localhost");
    public static final String MQTT_PORT = ParameterReader.getValue("MQTT_BROKER_PORT", "1883");

    // Distribution
    public static final String DISTRIBUTION_MA_TASKS_QUEUE = ParameterReader.getValue("MA_TASK_QUEUE", "MA_TASKS");
    public static final String DISTRIBUTION_MA_NEW_DEVICE_MESSAGE = ParameterReader.getValue("MA_NEW_DEVICE", "new Device");
    public static final String DISTRIBUTION_MA_DELETED_DEVICE_MESSAGE = ParameterReader.getValue("MA_DELETED_DEVICE", "deleted Device");

}
