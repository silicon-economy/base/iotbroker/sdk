/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.parameter;

import java.util.Map;

/**
 * This is a convenience class to read Strings and numbers from environment
 * variables.
 */
public class ParameterReader {

    private static final Map<String, String> ENV_MAP = System.getenv();

    private ParameterReader() {
        // Hide utility class constructor
    }

    public static String getValue(String envVar, String defaultValue) {
        return ENV_MAP.getOrDefault(envVar, defaultValue);
    }

    public static long getValue(String envVar, long defaultValue) {
        try {
            return Long.parseLong(getValue(envVar, null));
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    public static double getValue(String envVar, double defaultValue) {
        try {
            return Double.parseDouble(getValue(envVar, null));
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }
}
