/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.amqp;

import com.rabbitmq.client.BuiltinExchangeType;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;

/**
 * Defines the configuration of the AMQP exchange where messages regarding the device management
 * are published to. This includes the following message:
 * <ul>
 *     <li>{@link DeviceInstanceUpdate}</li>
 *     <li>{@link DeviceTypeUpdate}</li>
 * </ul>
 *
 * @author M. Grzenia
 */
public class DeviceManagementExchangeConfiguration {

    /**
     * The name of the exchange.
     */
    public static final String NAME = "device.management";
    /**
     * The exchange type.
     * Corresponds to {@link BuiltinExchangeType#TOPIC}.
     */
    public static final String TYPE = "topic";
    /**
     * The exchange durability.
     */
    public static final boolean DURABLE = true;
    /**
     * The format of the routing key used to publish {@link DeviceInstanceUpdate}s to the exchange.
     * <p>
     * The format is based on the following pattern:
     * {@literal deviceInstance.<deviceTypeIdentifier>.update}
     *
     * @see #formatDeviceInstanceUpdateRoutingKey(String)
     */
    public static final String ROUTING_KEY_DEVICE_INSTANCE_UPDATE_FORMAT = "deviceInstance.%s.update";
    /**
     * The format of the routing key used to publish {@link DeviceTypeUpdate}s to the exchange.
     * <p>
     * The format is based on the following pattern:
     * {@literal deviceType.<deviceTypeIdentifier>.update}
     *
     * @see #formatDeviceTypeUpdateRoutingKey(String)
     */
    public static final String ROUTING_KEY_DEVICE_TYPE_UPDATE_FORMAT = "deviceType.%s.update";

    private DeviceManagementExchangeConfiguration() {
    }

    /**
     * Convenience method for formatting the {@link #ROUTING_KEY_DEVICE_INSTANCE_UPDATE_FORMAT}.
     *
     * @param deviceTypeIdentifier The device type identifier to use.
     * @return The formatted string.
     */
    public static String formatDeviceInstanceUpdateRoutingKey(String deviceTypeIdentifier) {
        return String.format(ROUTING_KEY_DEVICE_INSTANCE_UPDATE_FORMAT, deviceTypeIdentifier);
    }

    /**
     * Convenience method for formatting the {@link #ROUTING_KEY_DEVICE_TYPE_UPDATE_FORMAT}.
     *
     * @param deviceTypeIdentifier The device type identifier to use.
     * @return The formatted string.
     */
    public static String formatDeviceTypeUpdateRoutingKey(String deviceTypeIdentifier) {
        return String.format(ROUTING_KEY_DEVICE_TYPE_UPDATE_FORMAT, deviceTypeIdentifier);
    }
}
