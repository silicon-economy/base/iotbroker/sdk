/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.couchdb;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.ektorp.ComplexKey;
import org.ektorp.CouchDbConnector;
import org.ektorp.ViewQuery;
import org.ektorp.impl.StdObjectMapperFactory;
import org.ektorp.support.CouchDbRepositorySupport;
import org.ektorp.support.DesignDocument;

import java.util.HashMap;
import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * Provides support for queries on global views for {@link PartitionedRepositorySupport}.
 * <p>
 * In order to perform global queries on partitioned databases we need this support class to
 * handle the generation of a global design document. All logic concerning global queries is
 * encapsulated here.
 *
 * @author D. Ronnenberg
 */
class GlobalRepositorySupport<T> extends CouchDbRepositorySupport<T> {
    /**
     * Suffix for the global design document ID.
     */
    private static final String GLOBAL_DESIGN_DOC_SUFFIX = "_global";
    /**
     * The object mapper to use for view queries.
     */
    private final ObjectMapper viewQueryObjectMapper;

    protected GlobalRepositorySupport(Class<T> type, CouchDbConnector db,
                                      String designDocName, boolean createIfNotExists,
                                      ObjectMapper viewQueryObjectMapper) {
        super(type, db, designDocName + GLOBAL_DESIGN_DOC_SUFFIX, createIfNotExists);

        if (viewQueryObjectMapper == null) {
            this.viewQueryObjectMapper = new StdObjectMapperFactory().createObjectMapper();
        } else {
            this.viewQueryObjectMapper = viewQueryObjectMapper;
        }
    }

    public void initGlobalDesignDocument(Object realRepository) {
        initDesignDocument(stdDesignDocumentId, getDesignDocumentFactory().generateGlobalFrom(realRepository));
    }

    /**
     * If needed, initializes or updates the given design documents in the database.
     *
     * @param designDocId          The ID of the design document in the database.
     * @param generatedDefaultDDoc The content of the generated default design document.
     */
    private void initDesignDocument(String designDocId, DesignDocument generatedDefaultDDoc) {
        DesignDocument designDoc;
        boolean update;
        if (db.contains(designDocId)) {
            designDoc = db.get(DesignDocument.class, designDocId);

            boolean changed = designDoc.mergeWith(generatedDefaultDDoc);
            // mergeWith does merge the anonymous part of the design doc (that's kinda hard). We only
            // check here if we have options and merge this.
            var merged = mergeOptions(generatedDefaultDDoc, designDoc);
            update = changed || merged;
        } else {
            designDoc = generatedDefaultDDoc;
            designDoc.setId(designDocId);
            update = true;
        }

        // Update the design document only if is has changed (because e.g. views were added, or it
        // did not previously exist) and it's not empty (in case a new one was created).
        if (update && !isDesignDocEmpty(designDoc)) {
            // This will throw an UpdateConflictException on error.
            db.update(designDoc);
        }
    }

    /**
     * Compares and updates the "options" of two design documents. The options are saved in the
     * anonymous part of the design document object.
     *
     * @param generated The generated default design document for this database.
     * @param designDoc The current state of the design document - as is in the database.
     * @return Indicates whether changes to designDoc has been made.
     */
    public static boolean mergeOptions(DesignDocument generated, DesignDocument designDoc) {
        var options = "options";
        HashMap<String, Object> generatedOptions = (HashMap<String, Object>) generated.getAnonymous().getOrDefault(options, new HashMap<String, Object>());
        HashMap<String, Object> existingOptions = (HashMap<String, Object>) designDoc.getAnonymous().getOrDefault(options, new HashMap<String, Object>());

        requireNonNull(generatedOptions);
        requireNonNull(existingOptions);

        // Merge the two hashmaps. Always use the values of "generated" in case of conflict.
        if (!existingOptions.equals(generatedOptions)) {
            generatedOptions.forEach((key, value) -> existingOptions.merge(key, value, (v1, v2) -> v2));
            designDoc.setAnonymous(options, existingOptions);

            return true;
        }
        return false;
    }

    @Override
    protected PartitionedDesignDocumentFactory getDesignDocumentFactory() {
        return new StdGlobalDesignDocumentFactory();
    }

    // Global query functions. Need to be public since the shims in PartitionedRepositorySupport
    // call them. That's why we need to override them here.
    @Override
    public ViewQuery createQuery(String viewName) {
        return new ViewQuery(viewQueryObjectMapper)
            .dbPath(db.path())
            .designDocId(stdDesignDocumentId)
            .viewName(viewName);
    }

    @Override
    public List<T> queryView(String viewName, String key) {
        return super.queryView(viewName, key);
    }

    @Override
    public List<T> queryView(String viewName, int key) {
        return super.queryView(viewName, key);
    }

    @Override
    public List<T> queryView(String viewName, ComplexKey key) {
        return super.queryView(viewName, key);
    }

    @Override
    public List<T> queryView(String viewName) {
        return super.queryView(viewName);
    }

    private boolean isDesignDocEmpty(DesignDocument designDoc) {
        return designDoc.getLists().isEmpty()
            && designDoc.getViews().isEmpty()
            && designDoc.getShows().isEmpty()
            && designDoc.getUpdates().isEmpty()
            && designDoc.getFilters().isEmpty();
    }
}
