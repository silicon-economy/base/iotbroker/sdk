/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.couchdb;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.ektorp.DbAccessException;
import org.ektorp.DbPath;
import org.ektorp.http.*;
import org.ektorp.impl.ObjectMapperFactory;
import org.ektorp.impl.StdCouchDbInstance;
import org.ektorp.impl.StdObjectMapperFactory;

import java.io.InputStream;

/**
 * The standard implementation of {@link PartitionedCouchDbInstance}.
 * <p>
 * This implementation extends {@link StdCouchDbInstance} and overrides some of its methods to allow
 * working with partitioned databases.
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
public class StdPartitionedCouchDbInstance
    extends StdCouchDbInstance
    implements PartitionedCouchDbInstance {

    /**
     * The rest template used to query the database.
     */
    private final RestTemplate restTemplate;
    /**
     * The object mapper instance to use for parsing JSON.
     */
    private final ObjectMapper objectMapper;

    public StdPartitionedCouchDbInstance(HttpClient client) {
        this(client, new StdObjectMapperFactory());
    }

    public StdPartitionedCouchDbInstance(HttpClient client, ObjectMapperFactory of) {
        super(client, of);

        this.restTemplate = new RestTemplate(client);
        this.objectMapper = new ObjectMapper();
    }

    @Override
    public boolean createDatabaseIfNotExists(DbPath db) {
        if (checkIfDbExists(db)) {
            return false;
        }

        var uri = String.format("/%s?partitioned=true", db.getDbName());
        return restTemplate.put(uri, new StdResponseHandler<Boolean>() {
            @Override
            public Boolean error(HttpResponse hr) {
                throw StdResponseHandler.createDbAccessException(hr);
            }

            @Override
            public Boolean success(HttpResponse hr) throws Exception {
                return StdResponseHandler.checkResponseBodyOkAndReturnDefaultValue(
                    hr,
                    true,
                    objectMapper
                );
            }
        });
    }

    @Override
    public boolean checkIfDbExists(DbPath db) {
        return restTemplate.get(db.getPath(), new StdResponseHandler<>() {
            @Override
            public Boolean error(HttpResponse hr) {
                if (hr.getCode() == HttpStatus.NOT_FOUND) {
                    return false;
                }
                throw StdResponseHandler.createDbAccessException(hr);
            }

            @Override
            public Boolean success(HttpResponse hr) throws Exception {
                InputStream content = hr.getContent();
                try {
                    // CouchDB will indicate whether a database is partitioned in the "props"
                    // field of the returned JSON. {@link https://docs.couchdb.org/en/latest/api/database/common.html#get--db}
                    JsonNode body = objectMapper.readTree(content);
                    JsonNode props = body.get("props");

                    if (props != null && props.isObject()) {
                        // Check if the "partitioned" property is set to true
                        JsonNode partitionedNode = props.get("partitioned");
                        boolean partitioned = partitionedNode != null
                            && partitionedNode.booleanValue();

                        if (!partitioned) {
                            throw new DbAccessException(
                                String.format(
                                    "Database \"%s\" exists but is not partitioned!",
                                    db.getDbName()
                                )
                            );
                        }

                        return true;
                    } else {
                        // "props" is not present
                        throw new DbAccessException(
                            String.format(
                                "Database \"%s\" exists but is not partitioned!",
                                db.getDbName()
                            )
                        );
                    }
                } finally {
                    IOUtils.closeQuietly(content);
                }
            }
        });
    }
}
