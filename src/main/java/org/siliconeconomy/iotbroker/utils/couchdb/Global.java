/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.couchdb;

import org.ektorp.support.View;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marker annotation to mark a {@link View} in a {@link PartitionedRepositorySupport} as a global
 * view.
 * <p>
 * In CouchDB, design documents in a partitioned database default to being partitioned and views
 * added to partitioned design documents can only be used for queries on one partition at a time.
 * For queries across multiple partitions a view in a <em>global</em> design document is required.
 * {@link PartitionedRepositorySupport} creates both a partitioned design document <em>and</em> a
 * global design document in a CouchDB database. In order for views to be added to the global
 * design document, this annotation is to be used in conjunction with {@link View}:
 * <pre>
 * {@literal @Global}
 * {@literal @View}(name = "myView", map = "...")
 * {@literal List<TestEntity>} myQuery(...) {
 *     var query = createQuery("myView");
 *
 *     // Further configuration of the query omitted...
 *
 *     return db.queryView(query, MyEntity.class);
 * }
 * </pre>
 * For methods that feature both annotations, the {@link PartitionedRepositorySupport} will not only
 * create a corresponding view in the (default) partitioned design document but also an identical
 * view in the global design document. The views in the global design document can then be used for
 * global queries across multiple partitions.
 *
 * @author D. Ronnenberg
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Global {
}
