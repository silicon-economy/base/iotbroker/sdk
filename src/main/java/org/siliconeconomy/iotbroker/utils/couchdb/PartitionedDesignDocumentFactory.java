/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.couchdb;

import org.ektorp.support.DesignDocument;
import org.ektorp.support.DesignDocumentFactory;

/**
 * Marker interface for a {@link DesignDocumentFactory} with support for partitioned databases.
 *
 * @author D. Ronnenberg
 */
interface PartitionedDesignDocumentFactory extends DesignDocumentFactory {
    /**
     * Generates a global design document with views generated and loaded according to the
     * annotations found in the {@code metaDataSource} object.
     *
     * @param metaDataSource the repository the design document is created for.
     * @return the created {@link DesignDocument}.
     */
    DesignDocument generateGlobalFrom(Object metaDataSource);
}
