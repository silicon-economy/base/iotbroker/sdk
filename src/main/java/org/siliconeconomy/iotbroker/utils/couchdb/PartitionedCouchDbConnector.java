/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.couchdb;

import org.ektorp.CouchDbConnector;

/**
 * Marker interface for a {@link CouchDbConnector} with support for partitioned databases.
 *
 * @author M. Grzenia
 */
public interface PartitionedCouchDbConnector extends CouchDbConnector {
}
