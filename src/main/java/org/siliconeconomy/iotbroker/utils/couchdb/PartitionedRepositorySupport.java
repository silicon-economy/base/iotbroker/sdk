/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.couchdb;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.ektorp.ComplexKey;
import org.ektorp.ViewQuery;
import org.ektorp.impl.StdObjectMapperFactory;
import org.ektorp.support.CouchDbRepositorySupport;

import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * Extends {@link CouchDbRepositorySupport} to provide access for partitioned databases in CouchDB.
 * <p>
 * Provides basic CRUD operations and helper functions to perform and create CouchDB views. The
 * missing functionality for partitioned databases is added by providing a custom implementation for
 * {@link #createQuery(String, String)}. This implementation is then used in the functions with
 * the name {@code queryPartitionedView} to achieve basic support.
 * {@link #createQuery(String, String)} can also be called directly and the return value can be
 * modified to construct more complex queries.
 *
 * @author D. Ronnenberg
 */
public class PartitionedRepositorySupport<T> extends CouchDbRepositorySupport<T> {

    /**
     * The URI schema for partitions in partitioned databases.
     */
    private static final String PARTITIONED_DESIGN_DOC_QUERY_FORMAT = "_partition/%s/%s";
    /**
     * The format of the IDs for design documents in the database.
     */
    private static final String DESIGN_DOC_ID_FORMAT = "_design/%s";

    /**
     * The name for the partitioned design document in the database.
     */
    private final String designDocId;
    /**
     * The object mapper to use for view queries.
     */
    private final ObjectMapper viewQueryObjectMapper;
    /**
     * Delegate to perform global queries.
     */
    private final GlobalRepositorySupport<T> globalRepositoryDelegate;

    /**
     * Main constructor. The interface has been slightly adjusted to accept an
     * {@link ObjectMapper} instance. This will be used when converting the parameters for the
     * query views.
     *
     * @param type                  The type of the entity that is used in CRUD operations/views.
     * @param db                    The database that the operations are being performed on.
     * @param designDocName         The name of the design document that will hold the views.
     * @param createIfNotExists     Flag whether to create the database if it is nonexistent.
     * @param viewQueryObjectMapper {@link ObjectMapper} to convert parameters of the
     *                              {@link ViewQuery} to JSON.
     */
    protected PartitionedRepositorySupport(Class<T> type, PartitionedCouchDbConnector db,
                                           String designDocName, boolean createIfNotExists,
                                           ObjectMapper viewQueryObjectMapper) {
        super(type, db, designDocName, createIfNotExists);
        requireNonNull(designDocName, "designDocName");
        // The handling (queries, ect.) of the global design doc is handled by this object.
        this.globalRepositoryDelegate = new GlobalRepositorySupport<>(
            type,
            db,
            designDocName,
            false,
            viewQueryObjectMapper);

        this.designDocId = String.format(DESIGN_DOC_ID_FORMAT, designDocName);

        if (viewQueryObjectMapper == null) {
            this.viewQueryObjectMapper = new StdObjectMapperFactory().createObjectMapper();
        } else {
            this.viewQueryObjectMapper = viewQueryObjectMapper;
        }
    }

    protected PartitionedRepositorySupport(Class<T> type, PartitionedCouchDbConnector db,
                                           String designDocName,
                                           ObjectMapper viewQueryObjectMapper) {
        this(type, db, designDocName, true, viewQueryObjectMapper);
    }

    protected PartitionedRepositorySupport(Class<T> type, PartitionedCouchDbConnector db,
                                           String designDocName) {
        this(type, db, designDocName, true, null);
    }

    /**
     * If needed, initializes two design documents in the database (global and partitioned) or
     * updates each individually.
     */
    @Override
    public void initStandardDesignDocument() {
        // Create/update design doc with partitioned functions.
        super.initStandardDesignDocument();

        // Create design doc with global functions.
        globalRepositoryDelegate.initGlobalDesignDocument(this);
    }

    /**
     * Creates a {@link ViewQuery} to perform queries on partitioned databases.
     *
     * @param viewName  The name of the partitioned view.
     * @param partition The name of the partition.
     * @return The {@link ViewQuery}.
     */
    protected ViewQuery createQuery(String viewName, String partition) {
        return new ViewQuery(viewQueryObjectMapper)
            .dbPath(db.path())
            .designDocId(String.format(PARTITIONED_DESIGN_DOC_QUERY_FORMAT, partition, designDocId))
            .viewName(viewName);
    }

    /**
     * Overloaded function to make use of {@link #createQuery(String, String)}.
     * <p>
     * Provides the same functionality as {@link #queryView(String, String)}, but for partitioned
     * databases.
     *
     * @see #queryView(String, String)
     */
    protected List<T> queryPartitionedView(String viewName, String partition, String key) {
        return db.queryView(
            createQuery(viewName, partition)
                .includeDocs(true)
                .key(key),
            type
        );
    }

    /**
     * Overloaded function to make use of {@link #createQuery(String, String)}.
     * <p>
     * Provides the same functionality as {@link #queryView(String, int)}, but for partitioned
     * databases.
     *
     * @see #queryView(String, int)
     */
    protected List<T> queryPartitionedView(String viewName, String partition, int key) {
        return db.queryView(
            createQuery(viewName, partition)
                .includeDocs(true)
                .key(key),
            type
        );
    }

    /**
     * Overloaded function to make use of {@link #createQuery(String, String)}.
     * <p>
     * Provides the same functionality as {@link #queryView(String, ComplexKey)}, but for
     * partitioned databases.
     *
     * @see #queryView(String, ComplexKey)
     */
    protected List<T> queryPartitionedView(String viewName, String partition, ComplexKey key) {
        return db.queryView(
            createQuery(viewName, partition)
                .includeDocs(true)
                .key(key),
            type
        );
    }

    /**
     * Overloaded function to make use of {@link #createQuery(String, String)}.
     * <p>
     * Provides the same functionality as {@link #queryView(String)}, but for partitioned databases.
     *
     * @see #queryView(String)
     */
    protected List<T> queryPartitionedView(String viewName, String partition) {
        return db.queryView(
            createQuery(viewName, partition)
                .includeDocs(true),
            type
        );
    }

    // Start of functions for performing global queries. These are just shims and delegate the
    // work to GlobalRepositorySupport.
    @Override
    protected ViewQuery createQuery(String viewName) {
        return globalRepositoryDelegate.createQuery(viewName);
    }

    @Override
    protected List<T> queryView(String viewName, String key) {
        return globalRepositoryDelegate.queryView(viewName, key);
    }

    @Override
    protected List<T> queryView(String viewName, int key) {
        return globalRepositoryDelegate.queryView(viewName, key);
    }

    @Override
    protected List<T> queryView(String viewName, ComplexKey key) {
        return globalRepositoryDelegate.queryView(viewName, key);
    }

    @Override
    protected List<T> queryView(String viewName) {
        return globalRepositoryDelegate.queryView(viewName);
    }
}
