/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.couchdb;

import org.ektorp.support.DesignDocument;
import org.ektorp.support.SimpleViewGenerator;
import org.ektorp.support.View;
import org.ektorp.util.ReflectionUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Generate views from conjunction of {@link Global} and {@link View} annotations.
 * <p>
 * This class will create the needed {@link DesignDocument.View} elements from methods with
 * both a {@link Global} and a {@link View} annotation. The aim is to reuse as much code as
 * possible/integrate as nicely as possible with Ektorp.
 *
 * @author D. Ronnenberg
 */
class GlobalViewGenerator extends SimpleViewGenerator {
    public Map<String, DesignDocument.View> generateGlobalViews(final Object repository) {
        final Map<String, DesignDocument.View> views = new HashMap<>();
        final Class<?> repositoryClass = repository.getClass();

        createDeclaredGlobalViews(views, repositoryClass);
        return views;
    }

    /**
     * Create global views.
     * <p>
     * Main function of this class. It inspects each method of a given class looking for a
     * combination of {@link View} and {@link Global} annotations. For these methods it will create
     * {@link DesignDocument.View} objects.
     *
     * @param views the container to store the generated views in.
     * @param klass the klaas that is being inspected/the views are being generated for.
     */
    private void createDeclaredGlobalViews(
        final Map<String, DesignDocument.View> views, final Class<?> klass) {

        // This is where the magic happens.
        ReflectionUtils.eachMethod(
            klass,
            method -> method.isAnnotationPresent(Global.class) && method.isAnnotationPresent(View.class)
        ).forEach(method -> addGlobalView(views, method.getAnnotation(View.class), klass));
    }

    // We need to provide this since SimpleViewGenerator::addView is private, thus we can't call it.
    private void addGlobalView(Map<String, DesignDocument.View> views, View input,
                               Class<?> repositoryClass) {
        // Load view from file.
        if (!input.file().isEmpty()) {
            views.put(input.name(), loadViewFromFile(views, input, repositoryClass));
            return;
        }

        // Load view from class path.
        if (shouldLoadFunctionFromClassPath(input.map())
            || shouldLoadFunctionFromClassPath(input.reduce())) {
            views.put(input.name(), loadViewFromFile(input, repositoryClass));
            return;
        }

        // Load view directly from the annotation - the code of the view is stored inside the
        // annotation as a string.
        views.put(input.name(), DesignDocument.View.of(input));
    }
}
