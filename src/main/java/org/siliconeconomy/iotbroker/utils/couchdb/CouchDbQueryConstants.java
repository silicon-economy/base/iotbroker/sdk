/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.couchdb;

/**
 * Defines constants that can be used for queries to CouchDB.
 *
 * @author M. Grzenia
 */
public class CouchDbQueryConstants {

    /**
     * Defines the string that represents a high key value.
     * When querying key ranges, this string can be used as a placeholder for high key values
     * resulting in the query to match and return entries up to the highest key value available.
     * Note that '{@code {}}' is no longer a suitable “high” key sentinel value.
     */
    public static final String HIGH_KEY_VALUE = "\ufff0";

    private CouchDbQueryConstants() {
    }
}
