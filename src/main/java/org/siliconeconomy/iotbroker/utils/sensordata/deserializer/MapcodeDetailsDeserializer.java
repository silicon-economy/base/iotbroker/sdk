/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.sensordata.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.siliconeconomy.iotbroker.model.sensordata.location.MapcodeDetails;
import org.siliconeconomy.iotbroker.model.sensordata.location.datatype.Mapcode;

import java.io.IOException;

/**
 * Deserializer for instances of {@link MapcodeDetails}.
 *
 * @author M. Grzenia
 */
public class MapcodeDetailsDeserializer extends StdDeserializer<MapcodeDetails> {

    public MapcodeDetailsDeserializer() {
        super(MapcodeDetails.class);
    }

    @Override
    public MapcodeDetails deserialize(JsonParser jsonParser,
                                      DeserializationContext context) throws IOException {
        ObjectMapper mapper = (ObjectMapper) jsonParser.getCodec();
        ObjectNode objectNode = mapper.readTree(jsonParser);

        return new MapcodeDetails(
            new Mapcode(
                mapper.treeToValue(objectNode.get("territory"), String.class),
                mapper.treeToValue(objectNode.get("code"), String.class)
            )
        );
    }
}
