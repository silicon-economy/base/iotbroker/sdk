/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.sensordata;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.siliconeconomy.iotbroker.model.sensordata.*;
import org.siliconeconomy.iotbroker.model.sensordata.location.LatitudeLongitudeDetails;
import org.siliconeconomy.iotbroker.model.sensordata.location.LocationEncodingType;
import org.siliconeconomy.iotbroker.model.sensordata.location.MapcodeDetails;
import org.siliconeconomy.iotbroker.model.sensordata.location.SimpleDetails;
import org.siliconeconomy.iotbroker.model.sensordata.observation.ObservationType;
import org.siliconeconomy.iotbroker.utils.sensordata.deserializer.*;
import org.siliconeconomy.iotbroker.utils.sensordata.serializer.ClassSerializer;
import org.siliconeconomy.iotbroker.utils.sensordata.serializer.LocationDetailsSerializer;
import org.siliconeconomy.iotbroker.utils.sensordata.serializer.ObservationResultSerializer;

/**
 * A {@link Module} with already registered serializers and deserializers needed for
 * serialization/deserialization of {@link SensorDataMessage} instances.
 * <p>
 * Note: In addition to registering this module, an {@link ObjectMapper} instance must be configured
 * with the {@link SerializationFeature#WRITE_DATES_AS_TIMESTAMPS} disabled and the
 * {@link JavaTimeModule} registered to properly serialize/deserialize instances of
 * {@link SensorDataMessage}.
 *
 * @author M. Grzenia
 */
public class SensorDataModule extends SimpleModule {

    public SensorDataModule() {
        addSerializer(Class.class, new ClassSerializer());
        addSerializer(LocationDetails.class, new LocationDetailsSerializer());
        addSerializer(ObservationResult.class, new ObservationResultSerializer());

        addDeserializer(Class.class, new ClassDeserializer());
        addDeserializer(Datastream.class, new DatastreamDeserializer());
        addDeserializer(ObservationType.class, new ObservationTypeDeserializer());
        addDeserializer(Location.class, new LocationDeserializer());
        addDeserializer(LocationEncodingType.class, new LocationEncodingTypeDeserializer());

        // Deserializers for specific location details types of non-primitive type
        addDeserializer(LatitudeLongitudeDetails.class, new LongitudeLatitudeDetailsDeserializer());
        addDeserializer(MapcodeDetails.class, new MapcodeDetailsDeserializer());
        addDeserializer(SimpleDetails.class, new SimpleDetailsDeserializer());

        // Deserializers for specific observation result types of non-primitive type
        // No deserializers yet.
    }
}
