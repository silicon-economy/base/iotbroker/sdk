/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.sensordata.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.siliconeconomy.iotbroker.model.sensordata.*;
import org.siliconeconomy.iotbroker.model.sensordata.observation.ObservationType;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Deserializer for instances of {@link Datastream}.
 * This deserializer is needed because a datastream contains contextual information about its
 * observations (i.e. the {@link ObservationType}), which in turn is needed to properly deserialize
 * the results of the observations.
 *
 * @author M. Grzenia
 */
@SuppressWarnings("java:S3740")
// The whole purpose of this deserializer is to handle types at runtime
public class DatastreamDeserializer
    extends StdDeserializer<Datastream<? extends ObservationResult<?>>> {

    public DatastreamDeserializer() {
        super(Datastream.class);
    }

    @Override
    public Datastream<? extends ObservationResult<?>> deserialize(
        JsonParser jsonParser,
        DeserializationContext context) throws IOException {
        ObjectMapper mapper = (ObjectMapper) jsonParser.getCodec();
        ObjectNode objectNode = mapper.readTree(jsonParser);

        ObservationType observationType
            = mapper.treeToValue(objectNode.get("observationType"), ObservationType.class);

        List<Observation<? extends ObservationResult<?>>> observations = new ArrayList<>();
        for (JsonNode observationNode : objectNode.get("observations")) {
            observations.add(
                mapObservation(mapper, observationNode, observationType.getObservationResultClass())
            );
        }

        return new Datastream(
            mapper.treeToValue(objectNode.get("observedProperty"), ObservedProperty.class),
            observationType.getObservationResultClass(),
            mapper.treeToValue(objectNode.get("unitOfMeasurement"), UnitOfMeasurement.class),
            observations
        );
    }

    private Observation<? extends ObservationResult<?>> mapObservation(
        ObjectMapper mapper,
        JsonNode observationNode,
        Class<? extends ObservationResult<?>> observationResultClass) throws JsonProcessingException {
        return new Observation<>(
            mapper.treeToValue(observationNode.get("timestamp"), Instant.class),
            mapper.treeToValue(observationNode.get("result"), observationResultClass),
            mapper.treeToValue(observationNode.get("location"), Location.class),
            mapper.treeToValue(observationNode.get("parameters"), Map.class)
        );
    }
}
