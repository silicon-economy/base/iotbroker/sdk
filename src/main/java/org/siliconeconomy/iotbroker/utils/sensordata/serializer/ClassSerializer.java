/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.sensordata.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.siliconeconomy.iotbroker.model.sensordata.LocationDetails;
import org.siliconeconomy.iotbroker.model.sensordata.ObservationResult;
import org.siliconeconomy.iotbroker.model.sensordata.location.LocationEncodingType;
import org.siliconeconomy.iotbroker.model.sensordata.observation.ObservationType;
import org.siliconeconomy.iotbroker.utils.sensordata.deserializer.ClassDeserializer;
import org.siliconeconomy.iotbroker.utils.sensordata.deserializer.LocationEncodingTypeDeserializer;
import org.siliconeconomy.iotbroker.utils.sensordata.deserializer.ObservationTypeDeserializer;

import java.io.IOException;

/**
 * Serializer for instances of {@link Class}.
 * <p>
 * Due to some restrictions related to generics, this serializer has to be used for serialization of
 * <ul>
 * <li>sub types of {@link LocationDetails} and mapping them to the corresponding
 * {@link LocationEncodingType}</li>
 * <li>and sub type of {@link ObservationResult} and mapping them to the corresponding
 * {@link ObservationType}.</li>
 * </ul>
 * <p>
 * All other classes are serialized using their canonical name.
 *
 * @author M. Grzenia
 * @see ClassDeserializer
 * @see LocationEncodingTypeDeserializer
 * @see ObservationTypeDeserializer
 */
@SuppressWarnings("java:S3740")
// The whole purpose of this serializer is to handle types at runtime
public class ClassSerializer extends StdSerializer<Class> {

    public ClassSerializer() {
        super(Class.class, false);
    }

    @Override
    public void serialize(Class value,
                          JsonGenerator generator,
                          SerializerProvider provider) throws IOException {
        if (LocationDetails.class.isAssignableFrom(value)) {
            serializeLocationDetailsClass(value, generator);
        } else if (ObservationResult.class.isAssignableFrom(value)) {
            serializeObservationResultClass(value, generator);
        } else {
            generator.writeString(value.getCanonicalName());
        }
    }

    private void serializeLocationDetailsClass(Class<?> value,
                                               JsonGenerator generator) throws IOException {
        for (LocationEncodingType type : LocationEncodingType.values()) {
            if (type.getLocationDetailsClass().isAssignableFrom(value)) {
                generator.writeString(type.name());
                return;
            }
        }

        throw new IllegalArgumentException(String.format(
            "Unhandled type of %s: %s",
            LocationDetails.class.getCanonicalName(),
            value.getCanonicalName()
        ));
    }

    private void serializeObservationResultClass(Class<?> value,
                                                 JsonGenerator generator) throws IOException {
        for (ObservationType type : ObservationType.values()) {
            if (type.getObservationResultClass().isAssignableFrom(value)) {
                generator.writeString(type.name());
                return;
            }
        }

        throw new IllegalArgumentException(String.format(
            "Unhandled type of %s: %s",
            ObservationResult.class.getCanonicalName(),
            value.getCanonicalName()
        ));
    }
}
