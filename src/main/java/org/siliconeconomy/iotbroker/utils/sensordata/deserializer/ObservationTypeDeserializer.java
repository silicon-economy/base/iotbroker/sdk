/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.sensordata.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.TextNode;
import org.siliconeconomy.iotbroker.model.sensordata.observation.ObservationType;
import org.siliconeconomy.iotbroker.utils.sensordata.serializer.ClassSerializer;

import java.io.IOException;

/**
 * Deserializer for instances of {@link ObservationType}.
 *
 * @author M. Grzenia
 * @see ClassSerializer
 */
public class ObservationTypeDeserializer extends StdDeserializer<ObservationType> {

    public ObservationTypeDeserializer() {
        super(ObservationType.class);
    }

    @Override
    public ObservationType deserialize(JsonParser jsonParser,
                                       DeserializationContext context) throws IOException {
        ObjectMapper mapper = (ObjectMapper) jsonParser.getCodec();
        TextNode objectNode = mapper.readTree(jsonParser);
        String stringToDeserialize = mapper.treeToValue(objectNode, String.class);

        return ObservationType.valueOf(stringToDeserialize);
    }
}
