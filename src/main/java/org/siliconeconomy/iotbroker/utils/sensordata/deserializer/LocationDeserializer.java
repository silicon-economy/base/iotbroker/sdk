/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.sensordata.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.siliconeconomy.iotbroker.model.sensordata.Location;
import org.siliconeconomy.iotbroker.model.sensordata.LocationDetails;
import org.siliconeconomy.iotbroker.model.sensordata.location.LocationEncodingType;

import java.io.IOException;

/**
 * Deserializer for instances of {@link Location}.
 *
 * @author M. Grzenia
 */
@SuppressWarnings("java:S3740")
// The whole purpose of this deserializer is to handle types at runtime
public class LocationDeserializer extends StdDeserializer<Location<? extends LocationDetails<?>>> {

    public LocationDeserializer() {
        super(Location.class);
    }

    @Override
    public Location<? extends LocationDetails<?>> deserialize(
        JsonParser jsonParser,
        DeserializationContext context) throws IOException {
        ObjectMapper mapper = (ObjectMapper) jsonParser.getCodec();
        ObjectNode objectNode = mapper.readTree(jsonParser);

        LocationEncodingType locationEncodingType
            = mapper.treeToValue(objectNode.get("encodingType"), LocationEncodingType.class);

        return new Location(
            mapper.treeToValue(objectNode.get("name"), String.class),
            mapper.treeToValue(objectNode.get("description"), String.class),
            locationEncodingType.getLocationDetailsClass(),
            mapper.treeToValue(
                objectNode.get("details"),
                locationEncodingType.getLocationDetailsClass()
            )
        );
    }
}
