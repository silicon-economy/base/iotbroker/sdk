/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.mqtt;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.siliconeconomy.iotbroker.utils.parameter.ParameterReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.Semaphore;

import static java.lang.Thread.sleep;

class MQTTClient implements MqttCallback, AutoCloseable {

    private static final Logger LOGGER = LoggerFactory.getLogger(MQTTClient.class);

    // Default max in flight is 10
    private static final int MAX_IN_FLIGHT = 16;

    private final String mqttClientId;
    private final String brokerAdress;
    private final int qos;

    private MqttAsyncClient client;
    private static final Semaphore semaphore = new Semaphore(MAX_IN_FLIGHT);

    private Map<String, Set<IMqttMessageListener>> handlers = new HashMap<>();
    private List<ConnectionLostListener> connectionLostListeners = new ArrayList<>();

    /**
     * Creates an MQTTClient with given prefix.
     * Connect needs to be called afterwards to connect.
     * Broker hostname and port will be loaded from environment variables
     * qos will be 2 by default
     * @param clientIdPrefix prefix for mqtt client id
     */
    public MQTTClient(String clientIdPrefix) {
        this(clientIdPrefix, ParameterReader.getValue("MQTT_BROKER_HOSTNAME", "localhost"),
                ParameterReader.getValue("MQTT_BROKER_PORT", 1883),2);
    }

    /**
     * Creates MqttClient with given Parameters.
     * Connect needs to called afterwards to connect.
     * @param clientIdPrefix prefix for mqtt client id
     * @param brokerHostname hostname of broker to connect to
     * @param brokerPort port of broker to connect to
     * @param qos quality of service
     */
    public MQTTClient(String clientIdPrefix, String brokerHostname, long brokerPort, int qos) {
        this.qos = qos;
        this.mqttClientId = clientIdPrefix + "_" + MqttClient.generateClientId(); //underscore is important to divide between adapter name and unique id. (look, Jira IOTBROKER-70)
        this.brokerAdress = "tcp://" + brokerHostname + ":" + brokerPort;
    }

    /**
     * Connects to in constructor specified broker.
     */
    public IMqttToken connect() throws MqttException {
        this.client = new MqttAsyncClient(brokerAdress, mqttClientId, new MemoryPersistence());

        LOGGER.debug("Connect to MQTT Broker: {}", brokerAdress);

        MqttConnectOptions connOpts = new MqttConnectOptions();
        connOpts.setKeepAliveInterval(60);
        connOpts.setCleanSession(true);
        connOpts.setMaxInflight(MAX_IN_FLIGHT);

        //connect is handled by mqttclientmanagement
        connOpts.setAutomaticReconnect(false);

        this.client.setCallback(this);
        return this.client.connect(connOpts);
    }

    /**
     * Publishes given message to given topic.
     * @param topic topic to publish to.
     * @param message message to publish.
     * @throws MqttException Thrown if an error occurs during publishing.
     */
    public synchronized void publishMQTTMessage(final String topic, final byte[] message) throws MqttException, InterruptedException {
        try {
            semaphore.acquire();
            this.client.publish(topic, message, this.qos, false).setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken imt) {
                    semaphore.release();
                }

                @Override
                public void onFailure(IMqttToken imt, Throwable thrwbl) {
                    LOGGER.error("Error while publishing mqtt-message", thrwbl);
                }
            });
        } catch (InterruptedException exc) {
            LOGGER.debug("Interrupted!", exc);
            // Restore interrupted state of the running thread
            Thread.currentThread().interrupt();
        }
    }

    /**
     * subscribes to given topic.
     * Subscription will survive reconnect.
     * @param topic topic to subscribe to
     * @param handler message handler for incoming messages.
     * @throws MqttException
     */
    // according https://github.com/eclipse/paho.mqtt.java/issues/378 no other way than own map
    public void subscribeToTopic(final String topic, final IMqttMessageListener handler) throws MqttException {
        LOGGER.debug("{}: MQTT subcription topic: {}", handler.getClass().getSimpleName(), topic);
        Set<IMqttMessageListener> oldListeners = handlers.getOrDefault(topic, new HashSet<>());
        oldListeners.add(handler);
        handlers.put(topic, oldListeners);
        if (oldListeners.size() == 1) { //topic not subscribed so far
            this.client.subscribe(topic, this.qos, (String string, MqttMessage mm) -> {
                for (IMqttMessageListener h : handlers.get(topic)) {
                    h.messageArrived(string, mm);
                }
            }).waitForCompletion();
        }
    }

    protected void unsubscribe(String topic, IMqttMessageListener handler) throws MqttException {
        Set<IMqttMessageListener> allHandlers = this.handlers.getOrDefault(topic, new HashSet<>());
        allHandlers.remove(handler);
        this.handlers.put(topic, allHandlers);
        if (allHandlers.isEmpty()) {
            this.client.unsubscribe(topic).waitForCompletion();
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken imdt) {
        LOGGER.debug("Delivery completed.");
    }

    @Override
    public void connectionLost(Throwable thrwbl){
        LOGGER.error("MQTT client lost connection: {}", this.getClass().getSimpleName());
        LOGGER.error("Reason: ", thrwbl);
        LOGGER.error("No automatic connect by paho client, bsut automatic reconnect by mqttClient");
        for (ConnectionLostListener listener : connectionLostListeners) {
            listener.mqttConnectionLost(thrwbl);
        }
        while (!client.isConnected()) {
            try {
                sleep(1000);
                client.connect().waitForCompletion();

            } catch (MqttException e) {
                LOGGER.error(e.getMessage(), e);
            } catch (InterruptedException e) {
                LOGGER.error(e.getMessage(), e);
                Thread.currentThread().interrupt();
            }
        }
        LOGGER.info("Connected again, subscribing to old topics...");
        try {
            subscribeToOldTopics(); //subscriptions not saved by paho, after reconnect
        } catch (MqttException e) {
            LOGGER.error(e.getMessage(), e);
        }
        for (ConnectionLostListener listener : connectionLostListeners) {
            listener.mqttConnectedAgain();
        }

    }

    private void subscribeToOldTopics() throws MqttException {
        for (Map.Entry<String, Set<IMqttMessageListener>> entry : handlers.entrySet()) {
            this.client.subscribe(entry.getKey(), this.qos, (String string, MqttMessage mm) -> {
                for (IMqttMessageListener h : entry.getValue()) {
                    h.messageArrived(string, mm);
                }
            }).waitForCompletion();
        }
    }

    public void addConnectionLostListener(ConnectionLostListener listener) {
        this.connectionLostListeners.add(listener);
    }

    public void removeConnectionLostListener(ConnectionLostListener listener) {
        this.connectionLostListeners.remove(listener);
    }

    public boolean isConnected() {
        if (client != null) {
            return this.client.isConnected();
        } else {
            return false;
        }
    }

    /**
     * publish last will and close client
     *
     * @throws MqttException
     */
    private void closeAsyncClient() throws MqttException {
        this.client.close();
    }

    @Override
    public void messageArrived(String topic, MqttMessage mm) {
        LOGGER.debug("Message on topic {} arrived: {}", topic, mm);
    }

    public String getMqttClientId() {
        return mqttClientId;
    }

    @Override
    public void close() throws MqttException {
        LOGGER.info("Going to stop MQTT client: {}", this.getClass().getSimpleName());
        this.closeAsyncClient();
    }
}
