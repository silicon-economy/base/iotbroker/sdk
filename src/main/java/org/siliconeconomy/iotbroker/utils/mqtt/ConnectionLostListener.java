/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.mqtt;

public interface ConnectionLostListener {
    void mqttConnectionLost(Throwable throwable);

    void mqttConnectedAgain();
}
