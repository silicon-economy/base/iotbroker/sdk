/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.siliconeconomy.iotbroker.utils.parameter.ParameterReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class MQTTClientManagement implements AutoCloseable {

    private static final Logger LOGGER = LoggerFactory.getLogger(MQTTClientManagement.class);

    private final MQTTClient client;

    private static MQTTClientManagement instance;
    private static final long RECONNECT_INTERVAL = 10000;
    private static final long GRACE_SLEEP = 100;

    private final SenderThread senderThread = new SenderThread();

    private MQTTClientManagement() {
        String prefix = ParameterReader.getValue("MQTT_CLIENT_ID_PREFIX", "");
        this.client = new MQTTClient(prefix);
        senderThread.start();
    }

    private MQTTClientManagement(String clientIdPrefix, String brokerHostname, long brokerPort) {
        this.client = new MQTTClient(clientIdPrefix, brokerHostname, brokerPort, 2);
    }

    /**
     * Publishes given message to given topic.
     *
     * @param topic   topic to publish to
     * @param message message to publish
     */
    public void publishMQTTMessage(final String topic, final String message) {
        this.publishMQTTMessage(topic, message.getBytes());
    }

    /**
     * Publishes given message to given topic.
     *
     * @param topic   topic to publish to
     * @param message message to publish
     */
    public void publishMQTTMessage(final String topic, final byte[] message) {
        senderThread.pushEventTopicMessage(new TopicMessage(topic, message));
    }

    /**
     * subscribes to given topic.
     *
     * @param topic   topic to subscribe to.
     * @param handler handler for incoming messages.
     * @throws MqttException
     */
    public void subscribeToTopic(final String topic, final IMqttMessageListener handler) throws MqttException {
        getClient().subscribeToTopic(topic, handler);
    }

    public void unsubscribeTopic(final String topic, IMqttMessageListener handler) throws MqttException {
        getClient().unsubscribe(topic, handler);
    }


    public String getMqttClientId() {
        return getClient().getMqttClientId();
    }

    /**
     * Helper function to sleep for a specified time.
     * <p>
     * Useful if being interrupted and waking early is not important and
     * cluttering code with useless exception handling is not desired.
     *
     * @param millis time to sleep in milliseconds
     */
    private static void sleepUnsafe(long millis) {
        try {
            sleep(millis);
        } catch (InterruptedException ex) {
            LOGGER.debug("Exception occurred while sleeping", ex);
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Get single instance with lazy instantiation
     *
     * @return singleton instance of MQTTClientManagement
     */
    public static synchronized MQTTClientManagement getInstance() {
        if (instance == null) {
            instance = new MQTTClientManagement();
        }
        return instance;
    }

    /**
     * create new instance with custom broker hostname and port
     *
     * @param clientIdPrefix id prefix
     * @param brokerHostname hostname of broker
     * @param brokerPort     port of broker
     * @return new instance which connects to given hostname and port
     */
    public static synchronized MQTTClientManagement createNewInstance(String clientIdPrefix, String brokerHostname, long brokerPort) {
        return new MQTTClientManagement(clientIdPrefix, brokerHostname, brokerPort);
    }


    @Override
    public void close() {
        this.senderThread.stopRunning();
    }

    public void join() throws InterruptedException {
        this.senderThread.join();
    }

    private MQTTClient getClient() {
        while (!isConnected()) {
            try {
                connect();
            } catch (MqttException ex) {
                LOGGER.error(ex.getMessage());
                LOGGER.info(String.format("Sleeping %dseconds, before connect", RECONNECT_INTERVAL / 1000));
                try {
                    // Avoid autoreconnect
                    // TODO: Clarify the need to close the connection in case of a failure to connect
                    client.close();
                } catch (MqttException ex1) {
                    // This is kind of expected here, since there will be no connection to close
                    LOGGER.debug(ex1.getMessage(), ex1);
                }
                sleepUnsafe(RECONNECT_INTERVAL);
            }
        }
        // TODO: Clarify the need to sleep here
        sleepUnsafe(GRACE_SLEEP);
        return client;
    }

    public boolean isConnected() {
        return client.isConnected();
    }

    /**
     * reconnects to mqttBroker. Blocks current thread until reconnection or throws exception
     *
     * @throws MqttException
     */
    public void connect() throws MqttException {
        client.connect().waitForCompletion();
    }


    class SenderThread extends Thread {

        public SenderThread() {
            // Intentionally empty
        }

        private final BlockingQueue<TopicMessage> messageQueue = new LinkedBlockingQueue<>();
        private boolean running = true;

        @Override
        public void run() {
            TopicMessage tm = null;
            while (running) {
                try {
                    //blocks until element is pushed, or fail early to check if we should still run
                    tm = messageQueue.poll(GRACE_SLEEP, TimeUnit.MILLISECONDS);
                    if (tm != null) {
                        getClient().publishMQTTMessage(tm.getTopic(), tm.getPayload());
                    }
                } catch (InterruptedException ex) {
                    LOGGER.debug(ex.getMessage(), ex);
                    Thread.currentThread().interrupt();
                } catch (MqttException ex) {
                    LOGGER.error(ex.getMessage(), ex);
                    pushEventTopicMessage(tm);
                }
                sleepUnsafe(GRACE_SLEEP);
            }
            try {
                getClient().close();
            } catch (MqttException exc) {
                LOGGER.error("Error closing client", exc);
            }
        }

        public void stopRunning() {
            this.running = false;
        }

        private void pushEventTopicMessage(TopicMessage tm) {
            this.messageQueue.add(tm);
        }
    }
}
