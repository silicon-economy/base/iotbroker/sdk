/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.utils.mqtt;

/**
 * Class to store messages for blocking queue in MqttClientManagement.
 *
 * @author smengers
 */
class TopicMessage {

    private final String topic;
    private final byte[] payload;

    public TopicMessage(String topic, byte[] payload) {
        this.topic = topic;
        this.payload = payload;
    }

    public String getTopic() {
        return topic;
    }

    public byte[] getPayload() {
        return payload;
    }

}
