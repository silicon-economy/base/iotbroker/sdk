/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.device;

import lombok.*;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.Set;

/**
 * A device type contains information concerning multiple devices of the same type.
 * <p>
 * A device type can be considered as a <em>logical</em> group of devices that are similar with
 * respect to one or more distinctive characteristics. Examples of such characteristics can be:
 * <ul>
 *     <li>The name of the device model.</li>
 *     <li>A revision or version of the device's hardware or software.</li>
 *     <li>Other device properties.</li>
 * </ul>
 * These characteristics, or even a combination of them, can be used to describe a particular type
 * of device. For example, if the revision of a device's hardware changes, the device could be
 * considered a different type than it was at a previous revision.
 * <p>
 * In any case, a device type is uniquely identifiable via its {@link #source} and
 * {@link #identifier}. Multiple device types with the same source and identifier are therefore not
 * allowed.
 *
 * @author M. Grzenia
 * @author D. Ronnenberg
 * @see DeviceInstance
 */
@RequiredArgsConstructor
@Getter
@With
@EqualsAndHashCode
@ToString
public class DeviceType {

    /**
     * A unique identifier for this {@link DeviceType} instance.
     * <p>
     * This field is not to be confused with the {@link #identifier} field.
     * While the id is used to uniquely identify a device type across <em>all</em> device
     * types, regardless of any other device type properties, the identifier can only be
     * used to uniquely identify a device type in the context of the {@link #source} it is
     * associated with.
     * <p>
     * Some APIs may support using this attribute as an alternative shortcut for accessing device
     * types.
     */
    @NonNull
    private final String id;
    /**
     * The source that the device type is associated with. (E.g. as a source of data.)
     */
    @NonNull
    private final String source;
    /**
     * A unique identifier for the device type. (Only unique in the context of the device type's
     * source.)
     */
    @NonNull
    private final String identifier;
    /**
     * A list of adapters (more specifically adapter identifiers) via which devices of this type are
     * provided within the IoT Broker.
     */
    @NonNull
    private final Set<String> providedBy;
    /**
     * A description for the device type.
     * <p>
     * Optional: May be an empty string.
     */
    @NonNull
    private final String description;
    /**
     * Whether the entire device type is enabled or not.
     * <p>
     * For devices that are associated with a disabled device type, incoming data will not be
     * processed by the associated adapters.
     */
    private final boolean enabled;
    /**
     * Whether new, as yet unknown {@link DeviceInstance}s associated with this device type should
     * be automatically registered with the IoT Broker.
     */
    private final boolean autoRegisterDeviceInstances;
    /**
     * Whether newly registered {@link DeviceInstance}s associated with this device type should be
     * enabled automatically.
     */
    private final boolean autoEnableDeviceInstances;
}
