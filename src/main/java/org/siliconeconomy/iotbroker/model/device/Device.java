/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.device;

import lombok.*;
import org.checkerframework.checker.nullness.qual.NonNull;

import static java.util.Objects.requireNonNull;

/**
 * Represents a device that is known to the IoT broker.
 * <p>
 * This class contains mandatory and optional information about a device. At least a {@link #source}
 * and a {@link #tenant} are required in order to be able to uniquely identify a device. For fields
 * containing optional information, most, if not all, are required need not be {@code null}. This
 * is done merely to avoid checks for {@code null}.
 *
 * @author M. Grzenia
 * @deprecated Use {@link DeviceInstance} instead.
 */
@RequiredArgsConstructor
@Getter
@With
@EqualsAndHashCode
@ToString
@Deprecated
public class Device {

    /**
     * The source that the device represents. (E.g. as a source of data/messages.)
     */
    @NonNull
    private final String source;
    /**
     * A unique identifier for the device. (E.g. the device ID. Only unique in the context of the
     * device's source.)
     */
    @NonNull
    private final String tenant;
    /**
     * A descriptive text for the device type. (E.g. Button, Tracker, etc.)
     * Optional: May be an empty string.
     */
    @NonNull
    private final String type;
    /**
     * A descriptive text for the device model. (E.g. the device's code name.)
     * Optional: May be an empty string.
     */
    @NonNull
    private final String model;
    /**
     * A description for the device.
     * Optional: May be an empty string.
     */
    @NonNull
    private final String description;
    /**
     * The device's hardware revision.
     * Optional: May be an empty string.
     */
    @NonNull
    private final String hardwareRevision;
    /**
     * The device's firmware version.
     * Optional: May be an empty string.
     */
    @NonNull
    private final String firmwareVersion;

    /**
     * Creates a new instance.
     * <p>
     * Convenience constructor for the <i>actual</i> mandatory fields. Sets appropriate values
     * (e.g. empty strings) for the optional fields.
     *
     * @param source The source that the device represents.
     * @param tenant A unique identifier for the device.
     */
    public Device(@NonNull String source, @NonNull String tenant) {
        this(source, tenant, "", "", "", "", "");
    }
}
