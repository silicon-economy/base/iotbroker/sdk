/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.device;

import lombok.*;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.time.Instant;

/**
 * A representation of a device that is known to the IoT Broker.
 * <p>
 * A device is defined by the following properties:
 * <ul>
 *     <li>It is uniquely identifiable via its {@link #source} and {@link #tenant}. Multiple devices
 *     with the same source and tenant are therefore not allowed.</li>
 *     <li>It is always associated with a specific {@link DeviceType}.</li>
 * </ul>
 * In addition to the properties listed above, this class contains information that is important in
 * the context of the IoT Broker (e.g. the registration time or whether the device is enabled) and
 * information describing details about the device itself (e.g. the device's firmware version).
 * <p>
 * For fields containing optional information, most, if not all, are required not to be
 * {@code null}. This is done merely to avoid checks for {@code null}.
 *
 * @author M. Grzenia
 * @author D. Ronnenberg
 */
@RequiredArgsConstructor
@Getter
@With
@EqualsAndHashCode
@ToString
public class DeviceInstance {

    /**
     * A unique identifier for this {@link DeviceInstance} instance.
     * <p>
     * This field is not to be confused with the {@link #tenant} field.
     * While the id is used to uniquely identify a device instance across <em>all</em> device
     * instances, regardless of any other device instance properties, the tenant can only be
     * used to uniquely identify a device instance in the context of the {@link #source} it is
     * associated with.
     * <p>
     * Some APIs may support using this attribute as an alternative shortcut for accessing device
     * instances.
     */
    @NonNull
    private final String id;
    /**
     * The source that the device is associated with. (E.g. as a source of data.)
     */
    @NonNull
    private final String source;
    /**
     * A unique identifier for the device. (E.g. the device ID. Only unique in the context of the
     * device's source.)
     */
    @NonNull
    private final String tenant;
    /**
     * The identifier of the {@link DeviceType} the device is associated with.
     */
    @NonNull
    private final String deviceTypeIdentifier;
    /**
     * The date and time at which the device was registered.
     */
    @NonNull
    private final Instant registrationTime;
    /**
     * The date and time when the device was last seen.
     * <p>
     * If there has been no communication with the device so far, the value is
     * {@link Instant#EPOCH}.
     */
    @NonNull
    private final Instant lastSeen;
    /**
     * Whether the device is enabled or not.
     * <p>
     * For disabled devices, incoming data will not be processed by their associated adapter.
     */
    private final boolean enabled;
    /**
     * A description for the device.
     * <p>
     * Optional: May be an empty string.
     */
    @NonNull
    private final String description;
    /**
     * The device's hardware revision.
     * <p>
     * Optional: May be an empty string.
     */
    @NonNull
    private final String hardwareRevision;
    /**
     * The device's firmware version.
     * Optional: May be an empty string.
     */
    @NonNull
    private final String firmwareVersion;
}
