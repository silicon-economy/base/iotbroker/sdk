/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.device;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.checkerframework.checker.nullness.qual.NonNull;

import static java.util.Objects.requireNonNull;

/**
 * Represents an update event emitted for {@link DeviceInstance}s.
 *
 * @author M. Grzenia
 * @author D. Ronnenberg
 */
@Getter
@EqualsAndHashCode
@ToString
public class DeviceInstanceUpdate {

    /**
     * The new state of the {@link DeviceInstance} for which this update event was created.
     */
    private final DeviceInstance newState;
    /**
     * The old state of the {@link DeviceInstance} for which this update event was created.
     */
    private final DeviceInstance oldState;
    /**
     * This update event's type.
     */
    @NonNull
    private final Type type;

    /**
     * Creates a new instance.
     *
     * @param newState The new state of the {@link DeviceInstance}.
     * @param oldState The old state of the {@link DeviceInstance}.
     * @param type     The type of the update event.
     * @throws IllegalArgumentException If either {@code newState} or {@code oldState} is
     *                                  {@code null} without {@code type} having the appropriate
     *                                  value.
     */
    public DeviceInstanceUpdate(DeviceInstance newState,
                                DeviceInstance oldState,
                                @NonNull Type type) {
        this.type = requireNonNull(type, "type");
        if (newState == null && type != Type.DELETED) {
            throw new IllegalArgumentException("newState is null but object has not been deleted.");
        }
        this.newState = newState;
        if (oldState == null && type != Type.CREATED) {
            throw new IllegalArgumentException("oldState is null but object has not been created.");
        }
        this.oldState = oldState;
    }

    /**
     * Indicates the type of the update event.
     */
    public enum Type {
        /**
         * Indicates that a {@link DeviceInstance} has been created.
         */
        CREATED,
        /**
         * Indicates that a {@link DeviceInstance} has been modified.
         */
        MODIFIED,
        /**
         * Indicates that a {@link DeviceInstance} has been deleted.
         */
        DELETED;
    }
}
