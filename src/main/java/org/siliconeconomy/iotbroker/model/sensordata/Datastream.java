/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.sensordata;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * A datastream groups a collection of {@link Observation}s measuring the same {@link ObservedProperty} which
 * corresponds to a specific type of data (e.g. ambient temperature, ambient humidity, CPU temperature or
 * battery voltage).
 * <p>
 * It references the {@link ObservationResult} class that is used for the collection of observations which allows
 * the actual result of an observation to be evaluated during serialization and deserialization. In addition, a
 * datastream defines a {@link UnitOfMeasurement} to specify the measured quantity.
 *
 * @param <T> The type of observations grouped by the datastream.
 * @author C. Hoppe
 * @author M. Grzenia
 */
@Getter
@EqualsAndHashCode
public class Datastream<T extends ObservationResult<?>> {

    /**
     * The property that was observed.
     */
    @NonNull
    private final ObservedProperty observedProperty;
    /**
     * The type of {@link Observation}s grouped by the datastream.
     */
    @NonNull
    private final Class<T> observationType;
    /**
     * The unit of measurement.
     * The unit of measurement applies to the result of an {@link Observation}.
     */
    @NonNull
    private final UnitOfMeasurement unitOfMeasurement;
    /**
     * A list of observations, each describing the act of measuring or otherwise determining the value of an
     * {@link ObservedProperty}.
     */
    @NonNull
    private final List<Observation<T>> observations;

    /**
     * Creates a new instance.
     *
     * @param observedProperty  The property that was observed.
     * @param observationType   The type of observations grouped by the datastream.
     * @param unitOfMeasurement The unit of measurement.
     * @param observations      The list of observations.
     */
    public Datastream(@NonNull ObservedProperty observedProperty,
                      @NonNull Class<T> observationType,
                      @NonNull UnitOfMeasurement unitOfMeasurement,
                      @NonNull List<Observation<T>> observations) {
        this.observedProperty = requireNonNull(observedProperty, "observedProperty");
        this.unitOfMeasurement = requireNonNull(unitOfMeasurement, "unitOfMeasurement");
        this.observationType = requireNonNull(observationType, "observationType");
        this.observations = new ArrayList<>(requireNonNull(observations, "observations"));
    }

    /**
     * Returns an unmodifiable view of the observations.
     *
     * @return An unmodifiable view of the observations.
     */
    @NonNull
    public List<Observation<T>> getObservations() {
        return Collections.unmodifiableList(this.observations);
    }
}
