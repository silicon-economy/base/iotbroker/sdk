/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.sensordata.observation;

import lombok.EqualsAndHashCode;
import org.siliconeconomy.iotbroker.model.sensordata.ObservationResult;

/**
 * Describes an observation result using a floating-point number.
 * This is the implementation for the {@link ObservationType#MEASUREMENT}.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
@EqualsAndHashCode
public class MeasurementResult implements ObservationResult<Double> {

    /**
     * The result as a floating-point number.
     */
    private final Double measurement;

    /**
     * Creates a new instance.
     *
     * @param measurement The result as a floating-point number. Can be {@code null}, if the result could not be
     *                    determined.
     */
    public MeasurementResult(Double measurement) {
        this.measurement = measurement;
    }

    @Override
    public Double getResult() {
        return measurement;
    }
}
