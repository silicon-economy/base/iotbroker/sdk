/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.sensordata;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * This class models (sensor) data sent by a device connected to the IoT Broker.
 * <p>
 * For every type of data (e.g. ambient temperature, ambient humidity, CPU temperature or battery
 * voltage) this class contains a {@link Datastream} which groups a collection of
 * {@link Observation}s measuring the same {@link ObservedProperty} which corresponds to a specific
 * type of data.
 * In addition, this class may contain information about the {@link Location} of the device when it
 * sent the data.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
@Getter
@EqualsAndHashCode
@ToString
public class SensorDataMessage {
    /**
     * A unique identifier for the message.
     */
    @NonNull
    private final String id;
    /**
     * Reference to the message that originally contained the sensor data.
     * Optional: May be an empty string if no reference back to the original device message is
     * necessary or exists.
     */
    @NonNull
    private final String originId;
    /**
     * The source that the device represents. (E.g. as a source of messages that contain data.)
     */
    @NonNull
    private final String source;
    /**
     * A unique identifier for the device. (E.g. the device ID.)
     * This identifier is only unique in the context of the device's source.
     */
    @NonNull
    private final String tenant;
    /**
     * The time the device sent the data.
     * This time may be identical to the time of one or multiple {@link Observation}s in one or
     * multiple {@link Datastream}s. However, this may not be the case for observations made by the
     * device before this time.
     */
    @NonNull
    private final Instant timestamp;
    /**
     * The (optional) location of the device.
     * If the device cannot determine its location, the value is {@code null}.
     */
    private final Location<? extends LocationDetails<?>> location;
    /**
     * A list of datastreams, each grouping a collection of {@link Observation}s measuring the same
     * {@link ObservedProperty}.
     */
    @NonNull
    private final List<Datastream<? extends ObservationResult<?>>> datastreams;

    /**
     * Creates a new instance.
     * <p>
     * This constructor is especially required for deserialization of {@link SensorDataMessage}s
     * from JSON. With the Jackson framework, this can usually be achieved simply by using
     * <i>traditional</i> setter methods. However, in order to preserve the immutability of
     * instances of this class, this is not possible here.
     *
     * @param id          A unique identifier for the message.
     * @param originId    Reference to the message that originally contained the sensor data. May
     *                    be an empty string if no reference back to the original device message is
     *                    necessary or exists.
     * @param source      The source that the device represents.
     * @param tenant      A unique identifier for the device.
     * @param timestamp   The time the device sent the data.
     * @param location    The (optional) location of the device.
     * @param datastreams The list of datastreams.
     */
    @JsonCreator
    public SensorDataMessage(
        @JsonProperty("id") @NonNull String id,
        @JsonProperty("originId") @NonNull String originId,
        @JsonProperty("source") @NonNull String source,
        @JsonProperty("tenant") @NonNull String tenant,
        @JsonProperty("timestamp") @NonNull Instant timestamp,
        @JsonProperty("location") Location<? extends LocationDetails<?>> location,
        @JsonProperty("datastreams") @NonNull Collection<Datastream<? extends ObservationResult<?>>> datastreams) {
        this.id = requireNonNull(id, "id");
        this.originId = requireNonNull(originId, "originId");
        this.source = requireNonNull(source, "source");
        this.tenant = requireNonNull(tenant, "tenant");
        this.timestamp = requireNonNull(timestamp, "timestamp");
        this.location = location;
        this.datastreams = new ArrayList<>(requireNonNull(datastreams, "datastreams"));
    }

    /**
     * Returns an unmodifiable view of the datastreams.
     *
     * @return An unmodifiable view of the datastreams.
     */
    @NonNull
    @SuppressWarnings("java:S1452")
    public List<Datastream<? extends ObservationResult<?>>> getDatastreams() {
        // Return an unmodifieableList and ignore S1452 expecting no write access to it
        return Collections.unmodifiableList(this.datastreams);
    }
}
