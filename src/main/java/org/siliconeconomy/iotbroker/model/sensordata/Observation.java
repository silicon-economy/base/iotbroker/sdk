/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.sensordata;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * An observation describes the act of measuring or otherwise determining the value of an
 * {@link ObservedProperty}.
 * <p>
 * It defines the time the observation happened, the {@link ObservationResult} that contains the
 * actual result of the observation and a set of generic parameters that can be used to attach more
 * information to the observation. In addition, an observation may contain information about the
 * {@link Location} where the observation happened.
 * <p>
 * In case the <em>actual result</em> of the observation could not be determined and the
 * {@link ObservationResult} contains a {@code null} value, additional information can be included
 * in the observation's parameters to describe this circumstance in more detail.
 *
 * @param <T> The type of the observation result.
 * @author C. Hoppe
 * @author M. Grzenia
 */
@Getter
@EqualsAndHashCode
public class Observation<T extends ObservationResult<?>> {

    /**
     * The time the observation happened.
     */
    @NonNull
    private final Instant timestamp;
    /**
     * The result of the observation.
     * <p>
     * Although the result object in an {@link Observation} can not be {@code null}, the <em>actual
     * result</em> contained in the {@link ObservationResult} can be {@code null}. This might be the
     * case, if the <em>actual result</em> could not be determined.
     */
    @NonNull
    private final T result;
    /**
     * The (optional) location where the observation happened.
     * <p>
     * The location of an observation may be identical to the location of the device that made the
     * observation. If the device that made the observation cannot determine the location of the
     * observation, the value is {@code null}.
     */
    private final Location<? extends LocationDetails<?>> location;
    /**
     * A map of generic parameters.
     * <p>
     * These parameters can be used to attach more information to the observation.
     */
    @NonNull
    private final Map<String, String> parameters;

    /**
     * Creates a new instance.
     *
     * @param timestamp  The time the observation happened.
     * @param result     The result of the observation.
     * @param location   The (optional) location where the observation happened.
     * @param parameters The map of generic parameters.
     */
    @JsonCreator
    public Observation(@JsonProperty("timestamp") @NonNull Instant timestamp,
                       @JsonProperty("result") @NonNull T result,
                       @JsonProperty("location") Location<? extends LocationDetails<?>> location,
                       @JsonProperty("parameters") @NonNull Map<String, String> parameters) {
        this.timestamp = requireNonNull(timestamp, "timestamp");
        this.result = requireNonNull(result, "result");
        this.location = location;
        this.parameters = new HashMap<>(requireNonNull(parameters, "parameters"));
    }

    /**
     * Returns the value of the parameter with the given key.
     *
     * @param key The key of the parameter.
     * @return The value of the parameter.
     */
    public String getParameter(String key) {
        return this.parameters.get(key);
    }

    /**
     * Returns an unmodifiable view of the observation's parameters.
     *
     * @return An unmodifiable view of the observation's parameters.
     */
    @NonNull
    public Map<String, String> getParameters() {
        return Collections.unmodifiableMap(parameters);
    }
}
