/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.sensordata.observation;

import lombok.EqualsAndHashCode;
import org.siliconeconomy.iotbroker.model.sensordata.ObservationResult;

/**
 * Describes an observation result using an integer.
 * This is the implementation for the {@link ObservationType#COUNT}.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
@EqualsAndHashCode
public class CountResult implements ObservationResult<Long> {

    /**
     * The result as an integer.
     */
    private final Long count;

    /**
     * Creates a new instance.
     *
     * @param count The result as an integer. Can be {@code null}, if the result could not be determined.
     */
    public CountResult(Long count) {
        this.count = count;
    }

    @Override
    public Long getResult() {
        return count;
    }
}
