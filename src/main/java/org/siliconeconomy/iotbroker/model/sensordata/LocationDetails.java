/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.sensordata;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.model.sensordata.location.LocationEncodingType;

/**
 * Represents the details of a {@link Location} for a specific {@link LocationEncodingType}.
 *
 * @param <T> The type of the details.
 * @author C. Hoppe
 * @author M. Grzenia
 */
public interface LocationDetails<T> {

    /**
     * Returns the location details.
     *
     * @return The location details.
     */
    @NonNull
    T getDetails();
}
