/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.sensordata.observation;

import lombok.EqualsAndHashCode;
import org.siliconeconomy.iotbroker.model.sensordata.ObservationResult;

/**
 * Describes an observation result using a truth value.
 * This is the implementation for the {@link ObservationType#TRUTH}.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
@EqualsAndHashCode
public class TruthResult implements ObservationResult<Boolean> {

    /**
     * The result as a truth value.
     */
    private final Boolean truth;

    /**
     * Creates a new instance.
     *
     * @param truth The result as a truth value. Can be {@code null}, if the result could not be determined.
     */
    public TruthResult(Boolean truth) {
        this.truth = truth;
    }

    @Override
    public Boolean getResult() {
        return truth;
    }
}
