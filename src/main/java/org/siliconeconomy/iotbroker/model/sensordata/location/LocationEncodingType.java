/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.sensordata.location;

import org.siliconeconomy.iotbroker.model.sensordata.Location;
import org.siliconeconomy.iotbroker.model.sensordata.LocationDetails;

/**
 * Defines the different encoding types for a {@link Location} and links them to the {@link LocationDetails} sub types
 * they are associated with.
 * <p>
 * During serialization and deserialization, any references to the different location details sub type classes are
 * mapped to the names of the enum elements.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
public enum LocationEncodingType {

    /**
     * A location that is encoded in the simplest and most generic way using a string.
     */
    SIMPLE(SimpleDetails.class),
    /**
     * A location that is encoded as geographic coordinates using latitude and longitude coordinates.
     */
    LATLONG(LatitudeLongitudeDetails.class),
    /**
     * A location that is encoded using a mapcode.
     */
    MAPCODE(MapcodeDetails.class);

    private final Class<? extends LocationDetails<?>> locationDetailsClass;

    LocationEncodingType(Class<? extends LocationDetails<?>> locationDetailsClass) {
        this.locationDetailsClass = locationDetailsClass;
    }

    /**
     * Returns the details type (of a location) the location encoding type is associated with.
     *
     * @return The details type (of a location) the location encoding type is associated with.
     */
    @SuppressWarnings("java:S1452")
    public Class<? extends LocationDetails<?>> getLocationDetailsClass() {
        // This returns just a Class object, ignore java:S1452
        return locationDetailsClass;
    }
}
