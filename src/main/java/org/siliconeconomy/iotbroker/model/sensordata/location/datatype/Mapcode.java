/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.sensordata.location.datatype;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.checkerframework.checker.nullness.qual.NonNull;

import static java.util.Objects.requireNonNull;

/**
 * Represents a mapcode.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
@Getter
@EqualsAndHashCode
public class Mapcode {

    /**
     * The territory information.
     */
    @NonNull
    private final String territory;
    /**
     * The code.
     */
    @NonNull
    private final String code;

    /**
     * Creates a new instance.
     *
     * @param territory The territory information.
     * @param code      The code.
     */
    public Mapcode(@NonNull String territory, @NonNull String code) {
        this.territory = requireNonNull(territory, "territory");
        this.code = requireNonNull(code, "code");
    }

    @Override
    public String toString() {
        return territory + " " + code;
    }
}
