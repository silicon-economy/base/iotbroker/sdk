/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.sensordata.observation;

import lombok.EqualsAndHashCode;
import org.siliconeconomy.iotbroker.model.sensordata.ObservationResult;

/**
 * Describes an observation result in the simplest and most generic way using a string.
 * This is the implementation for the {@link ObservationType#SIMPLE}.
 *
 * @author M. Grzenia
 */
@EqualsAndHashCode
public class SimpleResult implements ObservationResult<String> {

    /**
     * The result as a string.
     */
    private final String result;

    /**
     * Creates a new instance.
     *
     * @param result The result as a string. Can be {@code null}, if the result could not be
     *               determined.
     */
    public SimpleResult(String result) {
        this.result = result;
    }

    @Override
    public String getResult() {
        return result;
    }
}
