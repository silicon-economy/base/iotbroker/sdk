/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.sensordata.location;

import lombok.EqualsAndHashCode;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.model.sensordata.LocationDetails;

import static java.util.Objects.requireNonNull;

/**
 * Describes a location in the simplest and most generic way using a string.
 * Can be used to describe a logical position. This is the implementation for the {@link LocationEncodingType#SIMPLE}.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
@EqualsAndHashCode
public class SimpleDetails implements LocationDetails<String> {

    /**
     * The string describing the location.
     */
    @NonNull
    private final String location;

    /**
     * Creates a new instance.
     *
     * @param location The string describing the location.
     */
    public SimpleDetails(@NonNull String location) {
        this.location = requireNonNull(location, "location");
    }

    @Override
    @NonNull
    public String getDetails() {
        return location;
    }
}
