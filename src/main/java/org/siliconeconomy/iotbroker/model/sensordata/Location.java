/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.sensordata;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.checkerframework.checker.nullness.qual.NonNull;

import static java.util.Objects.requireNonNull;

/**
 * A location describes the location of a device (see {@link SensorDataMessage}) or the location where an
 * {@link Observation} happened.
 * <p>
 * The location of an observation may be identical to the location of the device that made the observation. However,
 * this is not always the case (e.g. in the case of remote sensing) and these two locations can therefore be different.
 * <p>
 * A location references the {@link LocationDetails} class that is used to describe the details of the actual location,
 * which allows the location details to be evaluated during serialization and deserialization.
 *
 * @param <T> The (encoding) type of the location details.
 * @author C. Hoppe
 * @author M. Grzenia
 */
@Getter
@EqualsAndHashCode
public class Location<T extends LocationDetails<?>> {

    /**
     * An label for the location, commonly a descriptive name.
     * May be an empty string.
     */
    @NonNull
    private final String name;
    /**
     * An description about the location.
     * May be an empty string.
     */
    @NonNull
    private final String description;
    /**
     * The (encoding) type of the location details.
     */
    @NonNull
    private final Class<T> encodingType;
    /**
     * The details of the actual location.
     */
    @NonNull
    private final T details;

    /**
     * Creates a new instance.
     *
     * @param name         An label for the location, commonly a descriptive name.
     * @param description  An description about the location.
     * @param encodingType The (encoding) type of the location details.
     * @param details      The details of the actual location.
     */
    public Location(@NonNull String name,
                    @NonNull String description,
                    @NonNull Class<T> encodingType,
                    @NonNull T details) {
        this.name = requireNonNull(name, "name");
        this.description = requireNonNull(description, "description");
        this.encodingType = requireNonNull(encodingType, "encodingType");
        this.details = requireNonNull(details, "details");
    }
}
