/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.sensordata;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.checkerframework.checker.nullness.qual.NonNull;

import static java.util.Objects.requireNonNull;

/**
 * An observed property describes the property observed in the {@link Observation}s of a {@link Datastream}.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
@Getter
@EqualsAndHashCode
public class ObservedProperty {

    /**
     * A label for the observed property, commonly a descriptive name.
     */
    @NonNull
    private final String name;
    /**
     * A description about the observed property.
     */
    @NonNull
    private final String description;

    /**
     * Creates a new instance.
     *
     * @param name        A label for the observed property, commonly a descriptive name.
     * @param description A description about the observed property.
     */
    @JsonCreator
    public ObservedProperty(@JsonProperty("name") @NonNull String name,
                            @JsonProperty("description") @NonNull String description) {
        this.name = requireNonNull(name, "name");
        this.description = requireNonNull(description, "description");
    }
}
