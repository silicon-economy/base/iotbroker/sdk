/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.sensordata.location.datatype;

import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Represents a tuple of latitude and longitude coordinates.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
@Getter
@EqualsAndHashCode
public class LatLong {

    /**
     * The latitude coordinate.
     */
    private final double latitude;
    /**
     * The longitude coordinate.
     */
    private final double longitude;

    /**
     * Creates a new instance.
     *
     * @param latitude  The latitude coordinate.
     * @param longitude The longitude coordinate.
     */
    public LatLong(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return this.latitude + "," + this.longitude;
    }
}
