/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.sensordata.location;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.model.sensordata.LocationDetails;
import org.siliconeconomy.iotbroker.model.sensordata.location.datatype.LatLong;

import static java.util.Objects.requireNonNull;

/**
 * Describes a location as geographic coordinates using latitude and longitude coordinates.
 * This is the implementation for the {@link LocationEncodingType#LATLONG}.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
@Getter
@EqualsAndHashCode
public class LatitudeLongitudeDetails implements LocationDetails<LatLong> {

    /**
     * The latitude and longitude coordinates.
     */
    @NonNull
    private final LatLong latlong;

    /**
     * Creates a new instance.
     *
     * @param latlong The latitude and longitude coordinates.
     */
    public LatitudeLongitudeDetails(@NonNull LatLong latlong) {
        this.latlong = requireNonNull(latlong, "latlong");
    }

    @Override
    @NonNull
    public LatLong getDetails() {
        return this.latlong;
    }
}
