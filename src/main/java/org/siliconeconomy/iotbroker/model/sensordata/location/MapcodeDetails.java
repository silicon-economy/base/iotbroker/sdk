/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.sensordata.location;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.model.sensordata.LocationDetails;
import org.siliconeconomy.iotbroker.model.sensordata.location.datatype.Mapcode;

import static java.util.Objects.requireNonNull;

/**
 * Describes a location using a mapcode.
 * This is the implementation for the {@link LocationEncodingType#MAPCODE}.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
@Getter
@EqualsAndHashCode
public class MapcodeDetails implements LocationDetails<Mapcode> {

    /**
     * The mapcode.
     */
    @NonNull
    private final Mapcode mapcode;

    /**
     * Creates a new instance.
     *
     * @param mapcode The mapcode.
     */
    public MapcodeDetails(@NonNull Mapcode mapcode) {
        this.mapcode = requireNonNull(mapcode, "mapcode");
    }

    @Override
    @NonNull
    public Mapcode getDetails() {
        return mapcode;
    }
}
