/*
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.model.sensordata;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.model.sensordata.observation.ObservationType;

import static java.util.Objects.requireNonNull;

/**
 * This class describes the unit of a measurement for {@link ObservationResult}s in a {@link Datastream}'s
 * {@link Observation}s.
 * <p>
 * Although a unit of measurement is usually provided for datastreams with an {@link ObservationType#MEASUREMENT}, it
 * is not necessarily limited to these types.
 * In general, a unit of measurement can be used to further specify the result of an observation.
 * In cases where the name or the symbol of a unit of measurement cannot be meaningfully expressed, the corresponding
 * values should be set to an empty string.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
@Getter
@EqualsAndHashCode
public class UnitOfMeasurement {

    /**
     * The full name of the unit of measurement.
     * If there is no unit name, the value is an empty string.
     */
    @NonNull
    private final String name;
    /**
     * The textual form of the unit symbol.
     * If there is no unit symbol, the value is an empty string.
     */
    @NonNull
    private final String symbol;

    /**
     * Creates a new instance.
     *
     * @param name   The full name of the unit of measurement.
     * @param symbol The textual form of the unit symbol.
     */
    @JsonCreator
    public UnitOfMeasurement(@JsonProperty("name") @NonNull String name,
                             @JsonProperty("symbol") @NonNull String symbol) {
        this.name = requireNonNull(name, "name");
        this.symbol = requireNonNull(symbol, "symbol");
    }
}
