# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [5.5.0] - 2022-03-30
### Added
- Introduce messages intended to be sent via AMQP that represent events emitted for changes to `org.siliconeconomy.iotbroker.model.device.DeviceInstance` and `org.siliconeconomy.iotbroker.model.device.DeviceType` instances.
- Add a configuration for the AMQP exchange where messages related to the new device registration process are published to.

## [5.4.1] - 2022-02-25
### Fixed
- Fix typo on an attribute of `DeviceInstance`.
  It is supposed to be `deviceTypeIdentifier`, not `deviceTypIdentifier`.

## [5.4.0] - 2022-02-22
### Added
- Data structures to be used with a new, more sophisticated device registration process.
  This process revolves around device instances and device types.

### Changed
- Deprecate `org.siliconeconomy.iotbroker.model.device.Device` in favor of `org.siliconeconomy.iotbroker.model.device.DeviceInstance` and `org.siliconeconomy.iotbroker.model.device.DeviceType`.

## [5.3.0] - 2022-02-15
### Added
- Add support for global queries in a partitioned CouchDB repository.
  For this purpose a new annotation `@Global` is added which can be used in conjunction with the `@View` annotation to mark a view as a global view.

## [5.2.1] - 2021-11-17
### Changed
- Adjust the style of license headers.
  Use the slash-star style to avoid dangling Javadoc comments.
  Using this style also prevents the license headers from being affected when reformatting code via the IntelliJ IDEA IDE.

## [5.2.0] - 2021-10-29
### Added
- This CHANGELOG file to keep track of the changes in this project.
- Extend the SensorData model by a SimpleResult type that can be used to describe generic observation results.

### Changed
- Introduce dedicated deserializers for LocationEncodingType and ObservationType instead of handling deserialization of these types in ClassDeserializer.
  Since both LocationEncodingType and ObservationType now have a _SIMPLE_ type, "generic" deserialization via the ClassDeserializer is no longer possible.
  Furthermore, having dedicated deserializers makes the deserialization process more explicit.
- Improve Javadoc documentation a bit and describe an observation's result more explicitly.

## [5.1.0] - 2021-10-14
This is the first public release.

The IoT Broker SDK (IoT Broker Software Development Kit) facilitates development of components that are intended to be deployed and run in the IoT Broker environment.

This release corresponds to IoT Broker v0.9.
