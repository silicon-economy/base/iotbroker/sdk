> This repository is no longer maintained.
> The IoT Broker and its core components (including the components in this repository) are now maintained in a single repository at: https://git.openlogisticsfoundation.org/silicon-economy/base/iotbroker/iotbroker

# IoT Broker SDK

The IoT Broker SDK (IoT Broker Software Development Kit) facilitates development of components that are intended to be deployed and run in the IoT Broker environment.
It provides, among other things:

* Definitions of data structures that are essential to the IoT Broker and commonly used across multiple IoT Broker core components.
  These data structures describe, for example, the messages used for inter-component communication or the data provided by some of the IoT Broker's core services.
* Interfaces and default implementations that help developers connect their components to the IoT Broker infrastructure.

The goal of the IoT Broker SDK is to help developers with the integration and accelerate the development of their custom components.

## Documentation

The documentation for the IoT Broker as a whole is maintained in the IoT Broker core project and contains sections describing the components included in this project.

For more details, please refer to the IoT Broker core project's `documentation` directory.

## Git tags

The Git tags denominating versions used in this project do not correspond to tags for releases of individual components included in this project.
Instead, since the IoT Broker is composed of several components which are distributed across multiple repositories, the version numbers used with the tags refer to specific versions of the IoT Broker as a whole.
Thus, a tag marks a revision that belongs to the referenced version of the IoT Broker.
Therefore, the version numbers used with the tags do not necessarily match with the version numbers of the individual components.

## Continuous integration/Continuous deployment (CI/CD)

Development in this project facilitates CI/CD pipelines (e.g., for execution of builds and tests, deployment of build artifacts), tools for static code analysis, etc. currently on Fraunhofer internal infrastructure.

The corresponding deployment descriptors and pipeline scripts are provided as examples.

## Licenses of third-party dependencies

The licenses used by this project's third-party dependencies are documented in the `third-party-licenses` directory.
This is done to encourage developers to check the licenses used by the third-party dependencies to ensure they do not conflict with the license of this project itself.
The directory contains the following files:

* `third-party-licenses.txt` - Contains the licenses used by this project's third-party dependencies.
  The content of this file is/can be generated.
* `third-party-licenses-complementary.txt` - Contains entries for third-party dependencies for which the licenses cannot be determined automatically.
  The content of this file is maintained manually.

### Generating third-party license reports

This project uses the [license-maven-plugin](https://github.com/mojohaus/license-maven-plugin) to generate a file containing the licenses used by the third-party dependencies.
The content of the `mvn license:add-third-party` Maven goal's output (`target/generated-sources/license/THIRD-PARTY.txt`) can be copied into `third-party-licenses/third-party-licenses.txt`.

Third-party dependencies for which the licenses cannot be determined automatically by the license-maven-plugin have to be documented manually in `third-party-licenses/third-party-licenses-complementary.txt`.
In the `third-party-licenses/third-party-licenses.txt` file these third-party dependencies have an "Unknown license" license.
